#!/usr/bin/env python3

"""
Unpredictables are non-recurrent transactions (one time payments, etc).
While they cannot be predicted, they can be averaged.
"""

import bisect
import datetime

from . import prediction
from . import transaction


# MAGIC(JFlesch): don't look back further than 3 months
# This is not (entirely) random: predictions need 3 points to start
# working --> assuming 1 paycheck a month --> 3 months
# And we don't want risking cumulating prediction+unpredictable
MAGIC_MAX_LOOKBACK = prediction.MAGIC_SHORT_MEAN_MONTHS * 31


class UnpredictableRule(transaction.TransactionGroup):
    def __init__(self, transactions, end_vdate=None):
        """
        Arguments:
            facts: transactions being used as base for this rule
        """
        assert(len(transactions) > 0)

        # make the year of the transactions irrelevant
        transactions = transactions[:]
        transactions.sort()

        super().__init__(transactions=transactions, end_vdate=end_vdate)

    @staticmethod
    def from_dict(d, transactions, *args, **kwargs):
        return UnpredictableRule(
            transactions=[
                transactions[tuple(t_id)] for t_id in d['transactions']
            ],
        )

    def to_dict(self):
        return super().to_dict()

    def __str__(self):
        return (
            'UnpredictableRule('
            'len(transactions)={}, '
            'mean_amount_per_day={}, '
            ')'
        ).format(
            len(self.transactions),
            self.mean_amount_per_day,
        )

    def __repr__(self):
        return str(self)

    def is_valid(self):
        return self.start_vdate != self.end_vdate

    @staticmethod
    def get_unpredictable_rules(
                transactions, predictions,
                end_date=datetime.datetime.now()
            ):
        transactions = set(transactions)

        # ignore transactions used by predictions
        for p in predictions:
            for t in p.facts:
                transactions.remove(t)

        transactions = list(transactions)
        transactions.sort()

        # ignore too old transactions
        start = end_date - datetime.timedelta(days=MAGIC_MAX_LOOKBACK)
        start_idx = bisect.bisect(
            [t.vdate for t in transactions], start
        )
        transactions = transactions[start_idx:]

        if len(transactions) <= 0:
            return {}

        # reorder by accounts
        t_by_accounts = {}
        for t in transactions:
            tba = t_by_accounts.get(t.account, [])
            tba.append(t)
            t_by_accounts[t.account] = tba

        # build rules
        out = {}
        for (account, transactions) in t_by_accounts.items():
            r = UnpredictableRule(
                transactions,
                end_vdate=end_date
            )
            if not r.is_valid():
                continue
            out[account] = r
        return out
