import datetime
import math
import re
import statistics

from . import sortable


class Account(sortable.Sortable):
    SORT_CRITERIAS = [
        'backend',
        'label',
        'id',
    ]

    def __init__(self, account_id, label, backend="none"):
        super().__init__(eq_id=account_id)
        self.backend = backend
        self.account_id = account_id
        self.label = label

    def __str__(self):
        return 'Account(id={}, label="{}", backend="{}")'.format(
            self.account_id, self.label, str(self.backend)
        )

    def __repr__(self):
        return str(self)

    def to_dict(self):
        return {
            "id": self.account_id,
            "label": self.label,
            "backend": self.backend,
        }

    @staticmethod
    def from_dict(d, *args, **kwargs):
        return Account(d['id'], d['label'], d['backend'])


class Balance(sortable.Sortable):
    SORT_CRITERIAS = [
        'account',
        'date',
        'amount',
    ]

    def __init__(self, account, date, amount):
        super().__init__(eq_id=account.eq_id)
        self.account = account
        self.amount = float(amount)
        self.date = date

    def __str__(self):
        return 'Balance(account="{}", date={}, amount={:.2f})'.format(
            self.account, self.date.date(), self.amount
        )

    def __repr__(self):
        return str(self)

    def to_dict(self):
        return {
            "account": self.account.account_id,
            "date": [
                self.date.year,
                self.date.month,
                self.date.day,
            ],
            "amount": self.amount
        }

    @staticmethod
    def from_dict(d, accounts, *args, **kwargs):
        return Balance(
            account=accounts[d['account']],
            date=datetime.datetime(
                year=d['date'][0],
                month=d['date'][1],
                day=d['date'][2],
            ),
            amount=d['amount'],
        )


class Transaction(sortable.Sortable):
    """
    Represents a bank transaction.
    Immutable.
    """

    RE_DROPPED_CHARS = re.compile("[^a-zA-Z]")
    RE_DROPPED_GARBAGE = re.compile("( X|XX+)")

    SORT_CRITERIAS = [
        'vdate', 'denominator', 'amount', 'label', 'transaction_id'
    ]

    def __init__(self, account, transaction_id, label, vdate, amount,
                 extras={}):
        super().__init__(eq_id=(
            vdate.year,
            vdate.month,
            vdate.day,
            amount,
            label,
        ))
        self.account = account
        self.transaction_id = transaction_id
        if not hasattr(vdate, 'hour'):
            vdate = datetime.datetime.combine(
                vdate, datetime.datetime.min.time()
            )
        self.vdate = vdate
        self.amount = float(amount)
        self.label = label.upper()
        self.denominator = self.get_denominator(self.label)
        self.extras = extras

    def to_dict(self):
        return {
            "account": self.account.account_id,
            "id": self.transaction_id,
            "vdate": (
                self.vdate.year,
                self.vdate.month,
                self.vdate.day
            ),
            "amount": self.amount,
            "label": self.label,
            "denominator": self.denominator,
            "extras": self.extras
        }

    @staticmethod
    def from_dict(d, accounts, *args, **kwargs):
        return Transaction(
            account=accounts[d['account']],
            transaction_id=d['id'],
            label=d['label'],
            vdate=datetime.datetime(
                year=d['vdate'][0],
                month=d['vdate'][1],
                day=d['vdate'][2],
            ),
            amount=d['amount'],
            extras=d['extras'],
        )

    @staticmethod
    def get_denominator(label):
        label = label.upper()
        label = Transaction.RE_DROPPED_GARBAGE.sub("", label)
        label = Transaction.RE_DROPPED_CHARS.sub(" ", label)
        words = [word for word in label.split() if len(word) > 2]
        out = []
        words_len = 0
        # MAGIC(Jflesch): min 5 char. ("SPOTIFY") || ("AMAZON")
        # If the denominator groups too many elements together, it's not
        # a big deal. Predictions should split them apart correctly. It will
        # just require more CPU time.
        while words_len < 5 and len(words) > 0:
            word = words.pop(0)
            if len(word) <= 2:
                continue
            out.append(word)
            words_len += len(word)
        return " ".join(out)

    def distance(self, other_transaction, scale_date_interval, scale_amount):
        """
        Return a distance using 2 axis: date and amount
        """
        if scale_date_interval == 0.0 or scale_amount == 0.0:
            # either very same date, or canceled transactions
            # --> unusable transactions
            return 999999999.0
        dist_date = (self.vdate - other_transaction.vdate).days
        dist_date /= scale_date_interval
        dist_amount = (self.amount - other_transaction.amount)
        dist_amount /= scale_amount
        return math.sqrt((dist_date ** 2) + (dist_amount ** 2))

    def __str__(self):
        vdate = self.vdate
        if hasattr(vdate, 'hour'):
            vdate = vdate.date()
        return (
            'Transaction(denominator="{}", vdate={}, '
            'amount={:.2f}, label="{}", extras={})'.format(
                self.denominator, vdate, self.amount, self.label,
                str(self.extras)
            )
        )

    def __repr__(self):
        return str(self)


class TransactionGroup(sortable.Sortable):
    """
    Represent similar transactions. They must share a common denominator.
    Immutable.
    """

    SORT_CRITERIAS = [
        'denominator',
        'start_vdate',
        'end_vdate',
    ]

    def __init__(self, transactions, start_vdate=None, end_vdate=None):
        super().__init__(eq_id=transactions[0].eq_id)
        transactions = sorted(transactions)
        self.transactions = transactions

        self.account = transactions[0].account
        self.denominator = transactions[0].denominator
        self.label = transactions[0].label

        self.start_vdate = start_vdate
        if self.start_vdate is None:
            self.start_vdate = transactions[0].vdate
        self.end_vdate = end_vdate
        if self.end_vdate is None:
            self.end_vdate = transactions[-1].vdate

        amounts = [t.amount for t in transactions]
        self.sum_amount = sum(amounts)
        self.mean_amount = 0
        self.stddev_amount = 0
        self.mean_day_interval = 0
        if len(amounts) > 0:
            self.mean_amount = statistics.mean(amounts)
        if len(amounts) >= 2:
            self.stddev_amount = statistics.stdev(amounts)
        if len(self.transactions) >= 2:
            self.mean_day_interval = statistics.mean(
                self._get_date_intervals(self.transactions)
            )

    def short_mean_amount(self, months=3):
        # Use only the most recent values: Values during the last <months>
        # (3) months. Makes sure there at least 3 values. If not, extend
        # the time period as much as required to get at least 3 values.
        rq_points = 3
        min_date = self.transactions[-1].vdate - datetime.timedelta(
            days=months * 31
        )
        amounts = []
        for t in reversed(self.transactions):
            if rq_points < 0 and t.vdate < min_date:
                # ignore too old values
                break
            amounts.append(t.amount)
        return statistics.mean(amounts)

    @property
    def mean_amount_per_day(self):
        days = (self.end_vdate - self.start_vdate).days
        if days == 0:
            return -1
        return self.sum_amount / days

    @staticmethod
    def _get_date_intervals(transactions):
        for idx in range(0, len(transactions) - 1):
            yield (transactions[idx + 1].vdate - transactions[idx].vdate).days

    def to_dict(self):
        return {
            "mean_amount_per_day": self.mean_amount_per_day,
            "start_vdate": [
                self.start_vdate.year,
                self.start_vdate.month,
                self.start_vdate.day,
            ],
            "end_vdate": [
                self.end_vdate.year,
                self.end_vdate.month,
                self.end_vdate.day,
            ],
            "transactions": [t.eq_id for t in self.transactions]
        }

    @staticmethod
    def from_dict(d, transactions, *args, **kwargs):
        return TransactionGroup(
            transactions=[transactions[t_id] for t_id in d['transactions']],
            start_vdate=datetime.datetime(
                year=d['start_vdate'][0],
                month=d['start_vdate'][1],
                day=d['start_vdate'][2],
            ),
            end_vdate=datetime.datetime(
                year=d['end_vdate'][0],
                month=d['end_vdate'][1],
                day=d['end_vdate'][2],
            ),
        )

    def __str__(self):
        return (
            'TransactionGroup('
            'denominator={}, '
            'len(transactions)={}, '
            'mean_amount={:.2f}, '
            'short_mean_amount={:.2f}, '
            'mean_day_interval={:.2f}, '
            'label={}'
            ')'
        ).format(
            self.denominator,
            len(self.transactions),
            self.mean_amount,
            self.short_mean_amount(months=3),
            self.mean_day_interval,
            self.label
        )

    @staticmethod
    def group_transactions(all_transactions):
        out = {}
        for transaction in all_transactions:
            tg_id = (transaction.account, transaction.denominator)
            if tg_id in out:
                out[tg_id].append(transaction)
            else:
                out[tg_id] = [transaction]
        r = [
            TransactionGroup(transactions) for transactions in out.values()
            if len(transactions) > 1
        ]
        r.sort()
        return r
