import itertools


class Sortable(object):
    def __init__(self, eq_id):
        self.eq_id = eq_id
        assert(hasattr(self, 'SORT_CRITERIAS'))

    def __hash__(self):
        return hash(self.eq_id)

    def __lt__(self, other):
        for criteria in itertools.chain(self.SORT_CRITERIAS, ['eq_id']):
            ours = getattr(self, criteria)
            theirs = getattr(other, criteria)
            if ours != theirs:
                return ours < theirs
        assert False, "SORT FAILED: {} == {}".format(self, other)

    def __le__(self, other):
        for criteria in itertools.chain(self.SORT_CRITERIAS, ['eq_id']):
            ours = getattr(self, criteria)
            theirs = getattr(other, criteria)
            if ours != theirs:
                return ours <= theirs
        assert False, "SORT FAILED: {} == {}".format(self, other)

    def __gt__(self, other):
        for criteria in itertools.chain(self.SORT_CRITERIAS, ['eq_id']):
            ours = getattr(self, criteria)
            theirs = getattr(other, criteria)
            if ours != theirs:
                return ours > theirs
        assert False, "SORT FAILED: {} == {}".format(self, other)

    def __ge__(self, other):
        for criteria in itertools.chain(self.SORT_CRITERIAS, ['eq_id']):
            ours = getattr(self, criteria)
            theirs = getattr(other, criteria)
            if ours != theirs:
                return ours >= theirs
        assert False, "SORT FAILED: {} == {}".format(self, other)

    def __eq__(self, other):
        return self.eq_id == other.eq_id

    def __ne__(self, other):
        return self.eq_id != other.eq_id
