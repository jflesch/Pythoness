#!/usr/bin/env python3

import datetime
import getpass
import json
import sys
import traceback

from weboob.core import Weboob
from weboob.capabilities.bank import CapBank

from pythoness.model import transaction


def main():
    wb = Weboob()
    wb.load_backends(CapBank)

    backend = wb[sys.argv[1]]
    backend.browser.username = sys.argv[2]
    outfile = sys.argv[3]

    backend.browser.password = getpass.getpass()

    print("Loading ...")

    nb_transactions = 0

    accounts = []
    wb_accounts = []
    balances = []
    for account in backend.iter_accounts():
        wb_accounts.append(account)
        b = account.balance
        account = transaction.Account(
            account_id=account.fullid,
            label=account.label,
            backend=sys.argv[1],
        )
        accounts.append(account)
        balances.append(transaction.Balance(
            account=account,
            date=datetime.datetime.now(),
            amount=b,
        ))

    out_transactions = []
    for (account, wb_account) in zip(accounts, wb_accounts):
        print("{} ...".format(wb_account))
        try:
            transactions = backend.iter_history(wb_account)
            for t in transactions:
                # print("---")
                # for field in t.iter_fields():
                #     print(field)
                nb_transactions += 1
                dates = [t.vdate, t.date]
                for date in dates:
                    if not hasattr(date, 'year'):
                        continue
                    break
                else:
                    print("Warning: ignoring transaction date because it has"
                          " no valid date: {}".format(t))
                out_t = transaction.Transaction(
                    account=account,
                    transaction_id="{}-{}".format(
                        account.account_id,
                        # t.fullid if t.fullid is not None
                        # else nb_transactions,
                        nb_transactions,
                    ),
                    label=t.label,
                    vdate=date,
                    amount=float(t.amount),
                    extras={"raw": t.raw},
                )
                # merge to any identical transactions (easiest way to manage
                # duplicates) (for instance: 2 internet lines with the same
                # provider)
                for other in out_transactions:
                    if other == out_t:
                        print("Merge duplicate: {}".format(other))
                        other.amount += out_t.amount
                        break
                else:
                    out_transactions.append(out_t)
        except Exception as exc:
            print("WARNING: {}".format(str(exc)))
            traceback.print_exc()

    print("{} accounts".format(len(accounts)))
    print("{} transactions".format(nb_transactions))

    print("Dumping ...")

    with open(outfile, "w") as fd:
        out = {
            'accounts': [a.to_dict() for a in accounts],
            'balances': [b.to_dict() for b in balances],
            'transactions': [t.to_dict() for t in out_transactions],
        }
        json.dump(out, fd, indent=2, separators=(',', ': '))

    print("All done")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Syntax: {} <backend> <username> <outfile>".format(
            sys.argv[0]
        ))
        sys.exit(1)
    main()
