#!/usr/bin/env python3

import sys

from weboob.core import Weboob
from weboob.capabilities.bank import (CapBank, CapBankTransfer)
from weboob.core.repositories import IProgress


CAPS = (CapBank, CapBankTransfer)


class ConsoleProgress(IProgress):
    def progress(self, percent, message):
        sys.stdout.write('=== [%3.0f%%] %s\n' % (percent*100, message))

    def error(self, message):
        sys.stderr.write('ERROR: %s\n' % message)

    def prompt(self, message):
        return input(message)


def main():
    wb = Weboob()

    print("Available backends:")
    print("")
    print("[Idx] [Installed] Name")
    print("----------------------")

    modules = wb.repositories.get_all_modules_info()
    indexed = {}
    for (idx, (name, minfo)) in enumerate(sorted(modules.items())):
        if not minfo.has_caps(CAPS):
            continue
        indexed[idx] = minfo
        assert(name == minfo.name)
        print("[{}] [{}] {}".format(
            idx, "X" if minfo.is_installed() else " ", name
        ))

    to_install = int(input("Backend to install ? "))
    minfo = indexed[to_install]
    if not minfo.is_installed():
        wb.repositories.install(minfo.name, ConsoleProgress())

    module = wb.modules_loader.get_or_load_module(minfo.name)
    config = module.config
    params = {}

    for (k, v) in module.config.items():
        print("Config: {}={} : set to 'P'".format(k, str(v)))
        # default = v.default
        params[k] = 'P'  # prompt for login and password

    config = config.load(wb, module.name, minfo.name, params, nofail=True)
    config.save(edit=False)


if __name__ == "__main__":
    main()
