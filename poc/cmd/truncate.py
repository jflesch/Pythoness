#!/usr/bin/env python3

import datetime
import json
import sys

from pythoness.model import transaction


def truncate(content, date):
    accounts = [transaction.Account.from_dict(a) for a in content['accounts']]
    accounts = {a.account_id: a for a in accounts}
    balances = [
        transaction.Balance.from_dict(b, accounts) for b in content['balances']
    ]
    balances = {b.account: b for b in balances}
    transactions = [
        transaction.Transaction.from_dict(t, accounts)
        for t in content['transactions']
    ]
    transactions.sort()

    for t in transactions[:]:
        if t.vdate <= date:
            continue
        print("Dropping transaction: {}".format(t))
        transactions.remove(t)
        balances[t.account].amount -= t.amount

    print("----")
    for (k, v) in balances.items():
        print("{}: {:.2f}".format(k, v.amount))

    content['transactions'] = [t.to_dict() for t in transactions]
    content['balances'] = [v.to_dict() for (k, v) in balances.items()]
    return content


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("{} <transaction file to truncate> <end date YYYY-MM-DD>".format(
              sys.argv[0]))
        sys.exit(1)
    with open(sys.argv[1], 'r') as fd:
        content = json.load(fd)
    date = datetime.datetime.strptime(sys.argv[2], "%Y-%m-%d")
    content = truncate(content, date)
    with open(sys.argv[1], 'w') as fd:
        json.dump(content, fd, indent=2, separators=(',', ': '))
