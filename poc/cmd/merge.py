#!/usr/bin/env python3

import json
import sys


from pythoness.model import transaction
from pythoness.model import prediction
from pythoness.model import unpredictable


TO_MERGE = [
    ('accounts', transaction.Account),
    ('balances', transaction.Balance),
    ('transactions', transaction.Transaction),
    ('prediction_rules', prediction.PredictionRule),
    ('unpredictable_rules', unpredictable.UnpredictableRule),
]


def obj_merge(current, filename, data):
    for (obj_name, obj_class) in TO_MERGE:
        if obj_name not in data:
            continue
        print("- {} ...".format(obj_name))

        data_objs = set()
        for d in data[obj_name]:
            o = obj_class.from_dict(d, **current)
            data_objs.add(o)

        current_objs = (
            set(current[obj_name].values()) if obj_name in current else set()
        )

        for data_obj in data_objs:
            # always use the latest one
            if data_obj in current_objs:
                current_objs.remove(data_obj)
            current_objs.add(data_obj)

        current_objs = {obj.eq_id: obj for obj in current_objs}
        current[obj_name] = current_objs
    return current


if __name__ == "__main__":
    if len(sys.argv) <= 2:
        print("Usage:")
        print(
            "    {} <input_file_1> [<input_file_2> [...]]"
            " <output_file>".format(
                sys.argv[0]
            )
        )
        sys.exit(1)

    objs = {}
    for filename in sys.argv[1:-1]:
        print("Loading {} ...".format(filename))
        with open(filename, 'r') as fd:
            data = json.load(fd)
        print("Merging {} ...".format(filename))
        objs = obj_merge(objs, filename, data)

    print("Writing {} ...".format(sys.argv[-1]))
    out = {}
    for (obj_name, obj_class) in TO_MERGE:
        if obj_name not in objs:
            continue
        o = list(objs[obj_name].values())
        o.sort()
        o = [obj.to_dict() for obj in o]
        out[obj_name] = o

    with open(sys.argv[-1], 'w') as fd:
        json.dump(out, fd, indent=2, separators=(',', ': '))
