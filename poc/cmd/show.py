#!/usr/bin/env python3

import collections
import json
import sys

from pythoness.model import transaction
from pythoness.model import prediction
from pythoness.model import unpredictable


def print_balances(balances):
    t = 0
    for (account, balance) in balances.items():
        print("{}: {:.2f}".format(account.label, balance))
        t += balance
    print("Total: {:.2f}".format(t))


def main(analysis_file):
    with open(analysis_file, 'r') as fd:
        j = json.load(fd)
    accounts = [transaction.Account.from_dict(a) for a in j['accounts']]
    accounts = {a.account_id: a for a in accounts}
    balances = [
        transaction.Balance.from_dict(b, accounts) for b in j['balances']
    ]
    balances = {b.account: b.amount for b in balances}
    transactions = [
        transaction.Transaction.from_dict(t, accounts)
        for t in j['transactions']
    ]
    transactions.sort()
    indexed_transactions = {t.eq_id: t for t in transactions}

    predictions = None
    if 'prediction_rules' in j:
        predictions = [
            prediction.PredictionRule.from_dict(p, indexed_transactions)
            for p in j['prediction_rules']
        ]

    unpredictable_rules = None
    if 'unpredictable_rules' in j:
        unpredictable_rules = {
            accounts[account_id]: unpredictable.UnpredictableRule.from_dict(
                d, indexed_transactions
            ) for (account_id, d) in j['unpredictable_rules'].items()
        }

    if predictions is not None:
        print("# Last transactions:")
        for i in range(0, 5):
            print(str(transactions[-i]))
        print("----")
    else:
        print("# Transactions:")
        for t in transactions:
            print(str(t))
        print("----")

    if unpredictable_rules is not None:
        print("----")
        print("")
        print("# Unpredictable")
        for (k, v) in unpredictable_rules.items():
            print("")
            print("{}: {}".format(k.label, str(v)))
            for t in v.transactions:
                print("  " + str(t))

    if predictions is not None:
        print("----")
        print("")
        print("# Predictions:")

        for p in predictions:
            print("")
            print(p)
            for t in p.facts:
                print(t)

    print("")
    print("=================================================")
    print("=================================================")
    print("=================================================")
    print("")

    totals = collections.defaultdict(lambda: {'neg': 0.0, 'pos': 0.0})

    if predictions is not None:
        print("# Prediction summary:")
        print("")
        mean_amount_per_day = {'neg': 0.0, 'pos': 0.0}
        for p in predictions:
            if p.has_ended:
                continue
            print(p)
            k = 'pos' if p.mean_amount_per_day >= 0 else 'neg'
            mean_amount_per_day[k] += p.mean_amount_per_day
            totals[p.account][k] += p.mean_amount_per_day
        print("=> Total per day: {:.2f} + {:.2f} = {:.2f}".format(
            mean_amount_per_day['pos'], mean_amount_per_day['neg'],
            mean_amount_per_day['pos'] + mean_amount_per_day['neg']
        ))
        print("=> Total per ~month: {:.2f} + {:.2f} = {:.2f}".format(
            mean_amount_per_day['pos'] * 30.42,
            mean_amount_per_day['neg'] * 30.42,
            (mean_amount_per_day['pos'] + mean_amount_per_day['neg']) * 30.42
        ))
        print("")

    if unpredictable_rules is not None:
        print("# Unpredictable summary")
        mean_amount_per_day = {'neg': 0.0, 'pos': 0.0}
        for (k, v) in unpredictable_rules.items():
            print("{}: {}".format(k.label, str(v)))
            k = 'pos' if v.mean_amount_per_day >= 0 else 'neg'
            mean_amount_per_day[k] += v.mean_amount_per_day
            totals[v.account][k] += v.mean_amount_per_day
        print("=> Total per day: {:.2f} + {:.2f} = {:.2f}".format(
            mean_amount_per_day['pos'], mean_amount_per_day['neg'],
            mean_amount_per_day['pos'] + mean_amount_per_day['neg']
        ))
        print("=> Total per ~month: {:.2f} + {:.2f} = {:.2f}".format(
            mean_amount_per_day['pos'] * 30.42,
            mean_amount_per_day['neg'] * 30.42,
            (mean_amount_per_day['pos'] + mean_amount_per_day['neg']) * 30.42
        ))
        print("")

    if predictions is not None:
        print("# Total per account")
        total = {'neg': 0.0, 'pos': 0.0}
        for (k, v) in totals.items():
            print("- {}:".format(k))
            print("  - per day: {:.2f} + {:.2f} = {:.2f}".format(
                v['pos'], v['neg'], v['pos'] + v['neg']
            ))
            print("  - per ~month: {:.2f} + {:.2f} = {:.2f}".format(
                v['pos'] * 30.42, v['neg'] * 30.42,
                (v['pos'] + v['neg']) * 30.42
            ))
            total['neg'] += v['neg']
            total['pos'] += v['pos']
        print("=> Total per day: {:.2f} + {:.2f} = {:.2f}".format(
            total['pos'], total['neg'], (total['pos'] + total['neg'])
        ))
        print("=> Total per ~month: {:.2f} + {:.2f} = {:.2f}".format(
            total['pos'] * 30.42,
            total['neg'] * 30.42,
            (total['pos'] + total['neg']) * 30.42
        ))
        print("")

    print("# Balance")
    print_balances(balances)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("{} <analysis json file>".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1])
