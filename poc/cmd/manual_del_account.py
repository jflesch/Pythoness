#!/usr/bin/env python3

import json
import shutil
import sys

from pythoness.model import transaction


DATE_FORMAT = "%Y-%m-%d"


def main(filename):
    shutil.copy(filename, filename + ".bk")
    with open(filename, 'r') as fd:
        data = json.load(fd)

    accounts = [
        transaction.Account.from_dict(a)
        for a in data['accounts']
    ]
    accounts = {account.account_id: account for account in accounts}
    balances = [
        transaction.Balance.from_dict(b, accounts)
        for b in data['balances']
    ]
    transactions = [
        transaction.Transaction.from_dict(t, accounts)
        for t in data['transactions']
    ]
    transactions.sort()

    print("# Accounts / Current Balances")
    for (idx, balance) in enumerate(balances):
        print("{} - {}".format(idx, balance))
    print("")

    account_idx = input("Account ? [0] ").strip()
    if account_idx == "":
        account_idx = 0
    account_idx = int(account_idx)
    account = balances.pop(account_idx).account
    accounts.pop(account.account_id)

    for t in transactions[:]:
        if t.account == account:
            transactions.remove(t)

    data['accounts'] = [a.to_dict() for a in accounts.values()]
    data['balances'] = [b.to_dict() for b in balances]
    data['transactions'] = [t.to_dict() for t in transactions]

    with open(filename, 'w') as fd:
        json.dump(data, fd, indent=2, separators=(',', ': '))


if __name__ == "__main__":
    if len(sys.argv) <= 1 or "-h" in sys.argv or "--help" in sys.argv:
        print("Syntax: {} <file>".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1])
