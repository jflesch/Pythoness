#!/usr/bin/env python3

import datetime
import json
import os
import sys

from pythoness.model import transaction


def main(filename, account_name):
    if not os.path.exists(filename):
        data = {
            'accounts': [],
            'balances': [],
            'transactions': [],
        }
    else:
        with open(filename, 'r') as fd:
            data = json.load(fd)
    account = transaction.Account(account_name, account_name)
    balance = transaction.Balance(
        account, datetime.datetime.now(), 0.0
    )
    data['accounts'].append(account.to_dict())
    data['balances'].append(balance.to_dict())
    with open(filename, 'w') as fd:
        json.dump(data, fd, indent=2, separators=(',', ': '))
    print("Done")


if __name__ == "__main__":
    if len(sys.argv) <= 2:
        print("Syntax: {} <file> <account_name>".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])
