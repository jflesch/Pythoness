#!/usr/bin/env python3

import os
import json
import sys
import multiprocessing


from pythoness.model import transaction
from pythoness.model import prediction
from pythoness.model import unpredictable


def worker(in_q, out_q):
    while True:
        group = in_q.get()
        if group is None:
            return
        predictions = prediction.PredictionRule.guess_all_rules(
            group.transactions
        )
        out_q.put((group, predictions))


def main():
    transactions = []
    accounts = {}
    with open(sys.argv[1], "r") as fd:
        j = json.load(fd)
        accounts = [transaction.Account.from_dict(a) for a in j['accounts']]
        accounts = {a.account_id: a for a in accounts}
        transactions = [
            transaction.Transaction.from_dict(t, accounts)
            for t in j['transactions']
        ]

    transactions.sort()

    indexed = {
        g: None
        for g in transaction.TransactionGroup.group_transactions(transactions)
    }

    job_queue = multiprocessing.Queue()
    out_queue = multiprocessing.Queue()

    procs = []
    for _ in range(0, os.cpu_count()):
        p = multiprocessing.Process(
            target=worker, args=(job_queue, out_queue)
        )
        p.start()
        procs.append(p)

    nb_groups = 0
    for group in indexed.keys():
        job_queue.put(group)
        nb_groups += 1

    for i in range(0, nb_groups):
        if sys.stdout.isatty():
            sys.stdout.write("\r{}/{}...".format(i, nb_groups))
            sys.stdout.flush()
        (group, predictions) = out_queue.get()
        indexed[group] = predictions
    if sys.stdout.isatty():
        sys.stdout.write('\r{}/{}\n'.format(nb_groups, nb_groups))

    for _ in procs:
        job_queue.put(None)

    for t in procs:
        t.join()

    prediction_facts = set()
    predictions = []
    for (group, ps) in indexed.items():
        for p in ps:
            prediction_facts.update(p.facts)
        predictions += ps

    unpredictable_rules = (
        unpredictable.UnpredictableRule.get_unpredictable_rules(
            transactions, predictions
        )
    )
    unpredictable_rules = {
        account.account_id: rule.to_dict()
        for (account, rule) in unpredictable_rules.items()
    }

    with open(sys.argv[1], "r") as fd:
        j = json.load(fd)
    j['prediction_rules'] = [p.to_dict() for p in predictions]
    j['unpredictable_rules'] = unpredictable_rules
    with open(sys.argv[2], "w") as fd:
        json.dump(j, fd, indent=2, separators=(",", ": "))

    print("All done")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Syntax: {} <in_dump.json> <out_analysis.json>".format(
            sys.argv[0]
        ))
        sys.exit(1)
    main()
