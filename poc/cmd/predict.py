#!/usr/bin/env python3

import datetime
import json
import sys

from pythoness.model import transaction
from pythoness.model import prediction
from pythoness.model import unpredictable


def print_balances(balances):
    t = 0
    print("=> Balances:")
    for (account, balance) in balances.items():
        print("   {}: {:.2f}".format(account.label, balance))
        t += balance
    print("=> Total: {:.2f}".format(t))


def main(analysis_file, end_date):
    with open(analysis_file, 'r') as fd:
        j = json.load(fd)
    accounts = [transaction.Account.from_dict(a) for a in j['accounts']]
    accounts = {a.account_id: a for a in accounts}
    balances = [
        transaction.Balance.from_dict(b, accounts) for b in j['balances']
    ]
    balances = {b.account: b.amount for b in balances}
    transactions = [
        transaction.Transaction.from_dict(t, accounts)
        for t in j['transactions']
    ]
    transactions.sort()
    indexed_transactions = {t.eq_id: t for t in transactions}
    predictions = [
        prediction.PredictionRule.from_dict(p, indexed_transactions)
        for p in j['prediction_rules']
    ]

    unpredictable_rules = {
        accounts[account_id]: unpredictable.UnpredictableRule.from_dict(
            d, indexed_transactions
        ) for (account_id, d) in j['unpredictable_rules'].items()
    }

    print("# Last transactions:")
    for i in range(0, 5):
        print(str(transactions[-i]))
    print("----")

    print("# Balance")
    print_balances(balances)

    new_transactions = []
    for p in predictions:
        new = p.predict(transactions[-1].vdate, end_date)
        new = [(n, p) for n in new]
        new_transactions += new
    new_transactions.sort()

    print("")
    print("# Predictions:")
    vdate = transactions[-1].vdate
    for (t, p) in new_transactions:
        print("")
        print("=== " + str(t.vdate.date()) + " ===")
        print(p)
        print("=> " + str(t))
        diff_days = (t.vdate.date() - vdate.date()).days
        for account in accounts.values():
            if t.account == account:
                balances[account] += t.amount
            if diff_days > 0 and account in unpredictable_rules:
                u = unpredictable_rules[account].mean_amount_per_day
                print(
                    "   + Unpredictable {}: {:.2f} x {} days = {:.2f}".format(
                        account.label, u, diff_days, u * diff_days
                    )
                )
                u *= diff_days
                balances[account] += u
        print_balances(balances)
        vdate = t.vdate

    # unpredictable spending up to the end date for predictions
    diff_days = (end_date.date() - vdate.date()).days
    if diff_days > 0:
        print("")
        print("  " + str(end_date.date()))
        for account in accounts.values():
            if diff_days > 0 and account in unpredictable_rules:
                u = unpredictable_rules[account].mean_amount_per_day
                print(
                    "   + Unpredictable {}: {:.2f} x {} days = {:.2f}".format(
                        account.label, u, diff_days, u * diff_days
                    )
                )
                u *= diff_days
                balances[account] += u
        print_balances(balances)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("{} <analysis json file> <end date>".format(sys.argv[0]))
        print("   end date: date up to which predictions must be made")
        print("             format: YYYY-MM-DD")
        sys.exit(1)
    main(sys.argv[1], datetime.datetime.strptime(sys.argv[2], "%Y-%m-%d"))
