#!/usr/bin/env python3

import datetime
import json
import random
import sys

from pythoness.model import transaction


DATE_FORMAT = "%Y-%m-%d"


def main(filename):
    with open(filename, 'r') as fd:
        data = json.load(fd)

    accounts = [
        transaction.Account.from_dict(a)
        for a in data['accounts']
    ]
    accounts = {account.account_id: account for account in accounts}
    balances = [
        transaction.Balance.from_dict(b, accounts)
        for b in data['balances']
    ]
    transactions = [
        transaction.Transaction.from_dict(t, accounts)
        for t in data['transactions']
    ]
    transactions.sort()

    print("# Transactions:")
    for t in transactions:
        print("{}: {}" .format(t.account, t))
    print("----")

    print("# Accounts / Current Balances")
    for (idx, balance) in enumerate(balances):
        print("{} - {}".format(idx, balance))
    print("")

    balance_idx = input("Account ? [0] ").strip()
    if balance_idx == "":
        balance_idx = 0
    balance_idx = int(balance_idx)
    balance_obj = balances[balance_idx]

    date = datetime.date.today()
    date = datetime.date.strftime(date, DATE_FORMAT)
    date_str = input("Date ? [{}] ".format(date)).strip()
    if date_str != "":
        date = date_str
    date = datetime.datetime.strptime(date, DATE_FORMAT)

    new_balance = float(input("New balance ? "))

    new_transaction = transaction.Transaction(
        account=balance_obj.account,
        transaction_id=random.randint(0, 0xFFFFFFFF),
        label="Balance set to {}".format(new_balance),
        vdate=date,
        amount=new_balance - balance_obj.amount
    )
    transactions.append(new_transaction)
    transactions.sort()

    balance_obj.date = transactions[-1].vdate
    balance_obj.amount = new_balance

    balances[balance_idx] = balance_obj
    data['balances'] = [b.to_dict() for b in balances]
    data['transactions'] = [t.to_dict() for t in transactions]

    with open(filename, 'w') as fd:
        json.dump(data, fd, indent=2, separators=(',', ': '))


if __name__ == "__main__":
    if len(sys.argv) <= 1 or "-h" in sys.argv or "--help" in sys.argv:
        print("Syntax: {} <file>".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1])
