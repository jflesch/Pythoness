import datetime
import unittest

import openpaperwork_core

import pythoness.model.unpredictable


generated = 0


def gen_id():
    global generated
    generated += 1
    return str(generated)


class UnpredictableRuleTestCase(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.prediction")
        self.core.load("pythoness.model.transactions")
        self.core.load("pythoness.model.unpredictable")
        self.core.init()

        self.dumb_account = self.core.call_success(
            "account_new", "DUMB_ACCOUNT", "DUMB_ACCOUNT"
        )

        pythoness.model.unpredictable.MAGIC_MAX_LOOPBACK = 3 * 31

    def test_get_unpredictable_bad_input(self):
        out = {}
        self.core.call_success("unpredictable_get_all_rules", out, [], [])
        self.assertEqual(out, {})

        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=3, day=1),
                amount=-30.0,
            ),
        ]
        out = {}
        self.core.call_success(
            "unpredictable_get_all_rules",
            out, transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(out, {})

    def test_get_unpredictable_simple(self):
        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=18),
                amount=-30.0,
            ),
        ]
        out = {}
        self.core.call_success(
            "unpredictable_get_all_rules",
            out, transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(len(out), 1)
        out = out[self.dumb_account]
        # computed over the last 3 months
        self.assertEqual(round(out.mean_amount_per_day, 2), -0.65)

    def test_get_unpredictable_strip_old(self):
        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2016, month=2, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=18),
                amount=-30.0,
            ),
        ]
        out = {}
        self.core.call_success(
            "unpredictable_get_all_rules",
            out, transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(len(out), 1)
        out = out[self.dumb_account]
        # computed over the last 3 months
        self.assertEqual(round(out.mean_amount_per_day, 2), -0.65)

    def test_get_unpredictable(self):
        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SPOTIFY FRANCE 75 PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-40.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=2, day=1),
                amount=-70.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=2, day=10),
                amount=-90.0,
            ),
        ]
        out = {}
        self.core.call_success(
            "unpredictable_get_all_rules",
            out, transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=2, day=10)
        )
        self.assertEqual(len(out), 1)
        out = out[self.dumb_account]
        # computed on 3 months
        self.assertEqual(round(out.mean_amount_per_day, 2), -2.47)
