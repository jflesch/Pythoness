import datetime
import unittest

import openpaperwork_core


class TestDb(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.transactions")
        self.core.load("pythoness.sqlite.transactions")
        self.core.init()

        (self.db_url, fd) = self.core.call_success(
            "fs_mktemp", prefix="pythoness_tests_", on_disk=True
        )
        fd.close()
        self.core.call_all("db_open", self.db_url)

    def tearDown(self):
        self.core.call_all("fs_unlink", self.db_url)

    def test_add_upd_del(self):
        account = self.core.call_success("account_new", "test", "test")
        self.core.call_all("db_account_add", account)

        transaction_a = self.core.call_success(
            "transaction_new",
            account=account,
            transaction_id="abc",
            label="def",
            vdate=datetime.datetime(year=1985, month=12, day=2),
            amount=150
        )
        transaction_b = self.core.call_success(
            "transaction_new",
            account=account,
            transaction_id=None,
            label="klm",
            vdate=datetime.datetime(year=1985, month=12, day=1),
            amount=-150
        )

        self.core.call_success("db_transactions_add", [
            transaction_a,
            transaction_b
        ])

        transactions = []
        self.core.call_all(
            "db_transactions_get", transactions,
            start_date=datetime.datetime(year=1985, month=11, day=15),
            end_date=None
        )
        self.assertEqual(transactions, [transaction_b, transaction_a])

        # update transaction_b.transactin_id
        (transaction_b, transaction_a) = transactions

        transactions = []
        self.core.call_all(
            "db_transactions_get_by_account", transactions,
            account=account,
            start_date=datetime.datetime(year=1985, month=11, day=15),
            end_date=None
        )
        self.assertEqual(transactions, [transaction_b, transaction_a])

        transaction_a.amount = 200
        self.core.call_success("db_transaction_upd", transaction_a)

        transactions = []
        self.core.call_all(
            "db_transactions_get", transactions,
            start_date=datetime.datetime(year=1985, month=11, day=15),
            end_date=None
        )
        self.assertEqual(transactions, [transaction_b, transaction_a])

        self.core.call_success("db_transaction_del", transaction_b)
        transactions = []
        self.core.call_all(
            "db_transactions_get", transactions,
            start_date=datetime.datetime(year=1985, month=11, day=15),
            end_date=None
        )
        self.assertEqual(transactions, [transaction_a])
