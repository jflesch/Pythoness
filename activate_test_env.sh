if ! [ -e sub/paperwork/Makefile ] ; then
	git submodule init
	git submodule update --remote --init
fi

if ! [ -d venv ]; then
	virtualenv -p python3 --system-site-packages venv
	# required for Paypal
	pip3 install pyexecjs
fi

source venv/bin/activate
