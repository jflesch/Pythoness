import logging
import sqlite3

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 1000

    def __init__(self):
        super().__init__()
        self.db_path = None

    def get_interfaces(self):
        return [
            'db',
            'db_open',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'fs',
                'defaults': ['openpaperwork_core.fs.python'],
            },
            {
                'interface': 'mainloop',
                'defaults': ['openpaperwork_core.mainloop.asyncio'],
            },
        ]

    def db_open(self, file_url):
        LOGGER.info("Opening database '%s'", file_url)
        self.db_path = self.core.call_success("fs_unsafe", file_url)
        if self.db_path is None:
            LOGGER.error("Failed to open '%s'", file_url)
        self.core.call_all("on_db_modified")

    def db_get_cursor(self):
        if self.db_path is None:
            return None
        return sqlite3.connect(self.db_path)

    def db_execute(self, db_cursor, query, *args, **kwargs):
        return self.core.call_success(
            "mainloop_execute",
            db_cursor.execute, query,
            *args, **kwargs
        )
