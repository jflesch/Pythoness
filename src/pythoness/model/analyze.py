import collections
import datetime
import itertools
import logging
import os
import random
import time

import openpaperwork_core

from .. import _


LOGGER = logging.getLogger(__name__)


class Analysis(object):
    def __init__(self, plugin, transactions):
        self.core = plugin.core
        self.plugin = plugin

        self.max_transactions = self.core.call_success(
            "config_get", "analysis_max_transactions"
        )

        self.transactions = transactions
        self.running = True
        self.done = -1
        self.total = -1
        self.predictions = collections.defaultdict(list)

        self._start = None

    def stop(self):
        self.running = False
        for name in self.plugin.work_queues:
            self.core.call_all("work_queue_cancel_all", name)

    def start(self):
        self._start = time.time()
        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "on_progress", "analysis", 0.0, _("Analysis: Grouping ...")
        )
        LOGGER.info(
            "Starting analysis of %d transactions",
            len(self.transactions)
        )
        self.transactions.sort()
        promise = self.core.call_success(
            "transaction_group_all_promise", self.transactions
        )
        promise = promise.then(self._dispatch)
        self.core.call_success(
            "work_queue_add_promise",
            random.choice(self.plugin.work_queues),
            promise
        )

    def _dispatch(self, groups):
        if not self.running:
            LOGGER.info("Analysis interrupted")
            self.core.call_success(
                "mainloop_execute", self.core.call_all,
                "on_progress", "analysis", 1.0
            )
            return
        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "on_progress", "analysis", 0.0,
            _("Analysis: Looking for recurrent transactions ...")
        )
        LOGGER.info("%d transaction groups found", len(groups))
        LOGGER.info("Looking for recurrent transactions ...")

        self.done = 0
        self.total = 0
        for (idx, group) in enumerate(groups):
            LOGGER.info(
                "Group '%s': %d transactions (max %d used)",
                group.denominator, len(group.transactions),
                self.max_transactions
            )
            self.total += 1
            promise = self.core.call_success(
                "prediction_guess_all_promise",
                group.transactions[-self.max_transactions:],
                running_cb=lambda: self.running
            )
            promise = promise.then(self._on_predictions)
            self.core.call_success(
                "work_queue_add_promise",
                self.plugin.work_queues[idx % len(self.plugin.work_queues)],
                promise
            )

    def _on_predictions(self, predictions):
        if not self.running:
            LOGGER.info("Analysis interrupted")
            self.core.call_success(
                "mainloop_execute", self.core.call_all,
                "on_progress", "analysis", 1.0
            )
            return

        if len(predictions) > 0:
            account = predictions[0].facts[0].account
            self.predictions[account] += predictions

        LOGGER.info("%d predictions found", len(predictions))
        for prediction in predictions:
            LOGGER.info("Prediction found: %s", prediction)

        self.done += 1
        LOGGER.info("Predictions: %d/%d", self.done, self.total)

        if self.done < self.total:
            self.core.call_success(
                "mainloop_execute", self.core.call_all,
                "on_progress", "analysis", self.done / (self.total + 1),
                _("Analysis: Looking for recurrent transactions ...")
            )
            return

        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "on_progress", "analysis", self.done / (self.total + 1),
            _("Analysis: Computing averages of other transactions ...")
        )
        LOGGER.info("Computing unpredictable transactions averages ...")
        all_predictions = itertools.chain(*list(self.predictions.values()))
        promise = self.core.call_success(
            "unpredictable_get_all_rules_promise",
            self.transactions, all_predictions
        )
        promise = promise.then(self._on_unpredictables)
        self.core.call_success(
            "work_queue_add_promise",
            random.choice(self.plugin.work_queues),
            promise
        )

    def _on_unpredictables(self, unpredictables):
        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "on_progress", "analysis", 1.0
        )
        if not self.running:
            LOGGER.info("Analysis interrupted")
            return
        for (k, v) in unpredictables.items():
            LOGGER.info("Unpredictable: %s: %s", k, v)
        LOGGER.info("Analysis done")
        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_analysis_done",
            self.predictions, unpredictables
        )
        self.plugin.analysis = None  # to free the memory

        diff = time.time() - self._start
        LOGGER.info("Analysis took %dmin %ds", diff / 60, diff % 60)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 100000

    def __init__(self):
        super().__init__()
        self.analysis = None
        self.work_queues = []
        self.predictions = {}
        self.unpredictables = {}

    def get_interfaces(self):
        return [
            'analyze',
            'transation_storage',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'config',
                'defaults': ['openpaperwork_core.config'],
            },
            {
                'interface': 'prediction',
                'defaults': ['pythoness.model.prediction'],
            },
            {
                'interface': 'transaction_groups',
                'defaults': ['pythoness.model.transaction_groups'],
            },
            # there is 'transaction_storage' too, but as we act as a
            # 'transaction_storage' ourselves, we cannot declare it.
            {
                'interface': 'work_queue',
                'defaults': ['openpaperwork_core.work_queue.default'],
            },
            {
                'interface': 'unpredictable',
                'defaults': ['pythoness.model.unpredictable'],
            },
        ]

    def init(self, core):
        super().init(core)

        opt = self.core.call_success(
            "config_build_simple", "analysis", "max_transactions", lambda: 300
        )
        self.core.call_all("config_register", "analysis_max_transactions", opt)

        for i in range(0, max(1, os.cpu_count() - 1)):
            name = "analyze_{}".format(i)
            self.core.call_all("work_queue_create", name, stop_on_quit=True)
            self.work_queues.append(name)

    def on_quit(self):
        if self.analysis is not None:
            self.analysis.stop()
            self.analysis

    def db_transactions_get_by_account(
            self, out: list, account,
            start_date=None, end_date=None):
        start_date = datetime.datetime.now() + datetime.timedelta(days=1)
        start_date = datetime.datetime.combine(
            start_date.date(), datetime.datetime.min.time()
        )
        if end_date is None or start_date >= end_date:
            return

        predictions = self.predictions.get(account, [])
        for prediction in predictions:
            p = list(prediction.predict(start_date, end_date))
            LOGGER.debug(
                "Adding %d predictions for %s-%s", len(p), start_date, end_date
            )
            out += p

        unpredictable = self.unpredictables.get(account, None)
        if unpredictable is None:
            return

        mean = unpredictable.mean_amount_per_day
        date = datetime.datetime.combine(
            start_date.date() + datetime.timedelta(days=1),
            datetime.datetime.min.time()
        )
        idx = 0
        r = []
        while date < end_date:
            r.append(self.core.call_success(
                "transaction_new",
                account=account,
                transaction_id="{}-unpredictable-{}".format(
                    account.account_id, idx
                ),
                label="",
                vdate=date,
                amount=mean,
            ))
            idx += 1
            date = date + datetime.timedelta(days=1)
        LOGGER.debug(
            "Adding %d unpredictables for %s-%s", len(r), start_date, end_date
        )
        out += r

    def db_transactions_get_balance(self, account, date=None):
        if date is None:
            return None
        now = datetime.datetime.now()
        if date <= now:
            return None
        future_transactions = []
        self.db_transactions_get_by_account(
            future_transactions, account, now, date
        )
        balance = account.current_balance
        for t in future_transactions:
            balance += t.amount
        return balance

    def on_analysis_done(self, predictions, unpredictables):
        self.predictions = predictions
        self.unpredictables = unpredictables
        self.core.call_all("on_db_modified")

    def analyze(self):
        if self.analysis is not None:
            self.analysis.stop()
            self.analysis = None

        self.predicitions = {}

        transactions = []
        self.core.call_all("db_transactions_get", transactions)

        self.analysis = Analysis(self, transactions)
        self.analysis.start()
