import openpaperwork_core

from . import common


class Account(common.Sortable):
    SORT_CRITERIAS = [
        'label',
        'id',
    ]

    def __init__(self, account_id, label, current_balance=0.0):
        super().__init__(eq_id=account_id)
        self.account_id = account_id
        self.label = label
        self.current_balance = current_balance

    def __str__(self):
        return 'Account(id={}, label="{}", balance={})'.format(
            self.account_id, self.label, self.current_balance
        )

    def __repr__(self):
        return str(self)

    def to_dict(self):
        return {
            "id": self.account_id,
            "label": self.label,
        }

    @staticmethod
    def from_dict(d, *args, **kwargs):
        return Account(d['id'], d['label'])


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['accounts']

    def account_new(self, account_id, label, current_balance=0.0):
        return Account(account_id, label, current_balance)
