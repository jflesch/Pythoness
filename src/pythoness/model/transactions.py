import collections
import datetime
import math
import re

import openpaperwork_core


from . import common


class Transaction(common.Sortable):
    """
    Represents a bank transaction.
    Immutable.
    """

    RE_DROPPED_CHARS = re.compile("[^A-Z]")
    RE_DROPPED_GARBAGE = re.compile("( X|XX+)")

    SORT_CRITERIAS = [
        'vdate', 'denominator', 'amount', 'label', 'transaction_id'
    ]

    def __init__(self, account, transaction_id, label, vdate, amount,
                 extras={}):
        label = label.upper()
        super().__init__(eq_id=(
            vdate.year,
            vdate.month,
            vdate.day,
            amount,
            label,
        ))
        self.account = account
        self.transaction_id = transaction_id
        if not hasattr(vdate, 'hour'):
            vdate = datetime.datetime.combine(
                vdate, datetime.datetime.min.time()
            )
        self._vdate = vdate
        self._amount = float(amount)
        self._label = label
        self.denominator = self.get_denominator(self._label)
        self.extras = extras

    def upd(self):
        self.denominator = self.get_denominator(self._label)
        super().upd(eq_id=(
            self._vdate.year,
            self._vdate.month,
            self._vdate.day,
            self._amount,
            self._label,
        ))

    def _get_vdate(self):
        return self._vdate

    def _set_vdate(self, vdate):
        if not hasattr(vdate, 'hour'):
            vdate = datetime.datetime.combine(
                vdate, datetime.datetime.min.time()
            )
        self._vdate = vdate
        self.upd()

    vdate = property(_get_vdate, _set_vdate)

    def _get_amount(self):
        return self._amount

    def _set_amount(self, amount):
        self._amount = amount
        self.upd()

    amount = property(_get_amount, _set_amount)

    def _get_label(self):
        return self._label

    def _set_label(self, label):
        self._label = label
        self.upd()

    label = property(_get_label, _set_label)

    def to_dict(self):
        return {
            "account": self.account.account_id,
            "id": self.transaction_id,
            "vdate": (
                self.vdate.year,
                self.vdate.month,
                self.vdate.day
            ),
            "amount": self.amount,
            "label": self.label,
            "denominator": self.denominator,
            "extras": self.extras
        }

    @staticmethod
    def from_dict(d, accounts, *args, **kwargs):
        return Transaction(
            account=accounts[d['account']],
            transaction_id=d['id'],
            label=d['label'],
            vdate=datetime.datetime(
                year=d['vdate'][0],
                month=d['vdate'][1],
                day=d['vdate'][2],
            ),
            amount=d['amount'],
            extras=d['extras'],
        )

    @staticmethod
    def get_denominator(label):
        label = label.upper()
        label = Transaction.RE_DROPPED_GARBAGE.sub("", label)
        label = Transaction.RE_DROPPED_CHARS.sub(" ", label)
        words = [word for word in label.split() if len(word) > 2]
        out = []

        out = words[:2]
        words = words[2:]

        words_len = sum((len(w) for w in out))

        # reorder remaining words: biggest first
        # we assume here that the longest word is the most significant one
        words.sort(key=lambda w: len(w), reverse=True)

        # MAGIC(Jflesch): min 5 char. ("SPOTIFY") || ("AMAZON")
        # If the denominator groups too many elements together, it's not
        # a big deal. Predictions should split them apart correctly. It will
        # just require more CPU time.
        while words_len <= 5 and len(words) > 0:
            word = words.pop(0)
            if len(word) <= 2:
                break
            out.append(word)
            words_len += len(word)
        return " ".join(out)

    def distance(self, other_transaction, scale_date_interval, scale_amount):
        """
        Return a distance using 2 axis: date and amount
        """
        if scale_date_interval == 0.0 or scale_amount == 0.0:
            # either very same date, or canceled transactions
            # --> unusable transactions
            return 999999999.0
        dist_date = (self.vdate - other_transaction.vdate).days
        dist_date /= scale_date_interval
        dist_amount = (self.amount - other_transaction.amount)
        dist_amount /= scale_amount
        return math.sqrt((dist_date ** 2) + (dist_amount ** 2))

    def __str__(self):
        vdate = self.vdate
        if hasattr(vdate, 'hour'):
            vdate = vdate.date()
        return (
            'Transaction(denominator="{}", vdate={}, '
            'amount={:.2f}, label="{}", extras={})'.format(
                self.denominator, vdate, self.amount, self.label,
                str(self.extras)
            )
        )

    def __repr__(self):
        return str(self)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.generated_ids = collections.defaultdict(lambda: 0)

    def get_interfaces(self):
        return ['transactions']

    def transaction_new(
            self, account, transaction_id, label, vdate, amount,
            extras={}):
        if transaction_id is None:
            transaction_id = self.transaction_get_id(
                account, label, vdate, amount
            )
        return Transaction(account, transaction_id, label, vdate, amount)

    def on_import_start(self):
        self.generated_ids = collections.defaultdict(lambda: 0)

    def transaction_get_id(self, account, label, vdate, amount):
        t_id = (
            account.account_id +
            "_" + label.upper() +
            "_{}_{}_{}".format(vdate.year, vdate.month, vdate.day) +
            "_{}".format(int(amount * 1000))
        )
        idx = self.generated_ids[t_id]
        self.generated_ids[t_id] = idx + 1
        return t_id + "_{}".format(idx)

    def transaction_distance(
            self, transaction, other_transaction, scale_date, scale_amount):
        return transaction.distance(
            other_transaction, scale_date, scale_amount
        )
