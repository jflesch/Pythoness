"""
Unpredictables are non-recurrent transactions (one time payments, etc).
While they cannot be predicted, they can be averaged.
"""

import bisect
import datetime
import logging

import openpaperwork_core

from . import prediction
from . import transaction_groups


LOGGER = logging.getLogger(__name__)


# MAGIC(JFlesch): don't look back further than 3 months
# This is not (entirely) random: predictions need 3 points to start
# working --> assuming 1 paycheck a month --> 3 months
MAGIC_MAX_LOOKBACK = prediction.MAGIC_SHORT_MEAN_MONTHS * 31


class UnpredictableRule(transaction_groups.TransactionGroup):
    def __init__(self, transactions, start_vdate=None, end_vdate=None):
        """
        Arguments:
            facts: transactions being used as base for this rule
        """
        assert(len(transactions) > 0)

        # make the year of the transactions irrelevant
        transactions = transactions[:]
        transactions.sort()

        super().__init__(
            transactions=transactions, start_vdate=start_vdate,
            end_vdate=end_vdate
        )

    @staticmethod
    def from_dict(d, transactions, *args, **kwargs):
        return UnpredictableRule(
            transactions=[
                transactions[tuple(t_id)] for t_id in d['transactions']
            ],
        )

    def to_dict(self):
        return super().to_dict()

    def __str__(self):
        return (
            'UnpredictableRule('
            'len(transactions)={}, '
            'mean_amount_per_day={}, '
            ')'
        ).format(
            len(self.transactions),
            self.mean_amount_per_day,
        )

    def __repr__(self):
        return str(self)

    def is_valid(self):
        return self.start_vdate != self.end_vdate


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['unpredictable']

    def unpredictable_new(self, transactions, end_vdate=None):
        return UnpredictableRule(transactions, end_vdate)

    def unpredictable_get_all_rules(
            self, out: dict, transactions, predictions, end_date=None):
        if end_date is None:
            end_date = datetime.datetime.now()

        transactions = set(transactions)

        # ignore transactions used by predictions
        for p in predictions:
            for t in p.facts:
                if t not in transactions:
                    # TODO(Jflesch): shouldn't happen
                    LOGGER.warning("Transation not found: %s", t)
                    continue
                transactions.remove(t)

        transactions = list(transactions)
        transactions.sort()

        # ignore too old transactions
        start = end_date - datetime.timedelta(days=MAGIC_MAX_LOOKBACK)
        start_idx = bisect.bisect(
            [t.vdate for t in transactions], start
        )
        transactions = transactions[start_idx:]

        if len(transactions) <= 1:
            return {}

        # reorder by accounts
        t_by_accounts = {}
        for t in transactions:
            tba = t_by_accounts.get(t.account, [])
            tba.append(t)
            t_by_accounts[t.account] = tba

        # build rules
        for (account, transactions) in t_by_accounts.items():
            r = UnpredictableRule(
                transactions,
                start_vdate=start,
                end_vdate=end_date
            )
            if not r.is_valid():
                continue
            out[account] = r

    def unpredictable_get_all_rules_promise(
            self, transactions, predictions, end_date=None):
        out = {}
        promise = openpaperwork_core.promise.ThreadedPromise(
            self.core, self.unpredictable_get_all_rules,
            args=(out, transactions, predictions, end_date)
        )
        promise = promise.then(lambda *args, **kwargs: out)
        return promise
