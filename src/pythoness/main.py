import argparse
import logging
import sys

import openpaperwork_core
import openpaperwork_gtk

from . import _


LOGGER = logging.getLogger(__name__)


BASE_PLUGINS = (
    openpaperwork_core.MINIMUM_CONFIG_PLUGINS +
    [
        'pythoness.app',
    ]
)


DEFAULT_PLUGINS = (
    openpaperwork_core.RECOMMENDED_PLUGINS +
    openpaperwork_gtk.CLI_PLUGINS +
    openpaperwork_gtk.GUI_PLUGINS + [
        'openpaperwork_core.i18n.python',
        'openpaperwork_core.work_queue.default',
        'openpaperwork_gtk.dialogs.yes_no',
        'openpaperwork_gtk.dialogs.single_entry',
        'openpaperwork_gtk.widgets.calendar',
        'openpaperwork_gtk.widgets.charts.lines',
        'openpaperwork_gtk.widgets.progress',
        'pythoness.gui.accounts',
        'pythoness.gui.analyze',
        'pythoness.gui.balance',
        'pythoness.gui.db.auto_reopen',
        'pythoness.gui.db.open',
        'pythoness.gui.filters',
        'pythoness.gui.mainwindow',
        'pythoness.gui.transaction',
        'pythoness.gui.transaction_chart',
        'pythoness.gui.transaction_table',
        'pythoness.gui.transactions',
        'pythoness.gui.weboob.ask',
        'pythoness.gui.weboob.menu_item_import',
        'pythoness.gui.weboob.menu_item_settings',
        'pythoness.gui.weboob.menu_item_update',
        'pythoness.gui.weboob.settings',
        'pythoness.import',
        'pythoness.model.accounts',
        'pythoness.model.analyze',
        'pythoness.model.prediction',
        'pythoness.model.transaction_groups',
        'pythoness.model.transactions',
        'pythoness.model.unpredictable',
        'pythoness.sqlite.accounts',
        'pythoness.sqlite.db',
        'pythoness.sqlite.transactions',
        'pythoness.weboob',
        'pythoness.weboob.accounts',
        'pythoness.weboob.backends',
        'pythoness.weboob.modules',
    ]
)


def main_main(in_args):
    core = openpaperwork_core.Core()
    for module_name in BASE_PLUGINS:
        core.load(module_name)
    core.init()

    core.call_all(
        "init_logs", "pythoness",
        "info" if len(in_args) <= 0 else "warning"
    )

    core.call_all("config_load")
    core.call_all("config_load_plugins", "pythoness", DEFAULT_PLUGINS)

    if len(in_args) <= 0:

        core.call_all("on_initialized")

        LOGGER.info("Ready")
        core.call_one("mainloop", halt_on_uncaught_exception=False)
        LOGGER.info("Quitting")
        core.call_all("config_save")
        core.call_all("on_quit")

    else:

        parser = argparse.ArgumentParser()
        cmd_parser = parser.add_subparsers(
            help=_('command'), dest='command', required=True
        )

        core.call_all("cmd_complete_argparse", cmd_parser)
        args = parser.parse_args(in_args)

        core.call_all("cmd_set_interactive", True)

        r = core.call_all("cmd_run", args)
        if r <= 0:
            print("Unknown command or argument(s): {}".format(in_args))
            sys.exit(1)
        core.call_all("on_quit")
        return r


def main():
    main_main(sys.argv[1:])


if __name__ == "__main__":
    main()
