import logging

import openpaperwork_core

from . import modules


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['weboob_backends']

    def get_deps(self):
        return [
            {
                'interface': 'weboob',
                'defaults': ['pythoness.weboob'],
            },
        ]

    def weboob_backend_create(self, module_name, backend_name, params=None):
        if params is None:
            params = {}

        wb = self.core.call_success("weboob_get")
        module = wb.modules_loader.get_or_load_module(module_name)
        config = module.config.load(
            wb, module.name, backend_name, params, nofail=True
        )
        for (k, v) in params.items():
            if k not in config:
                continue
            config[k].set(v)
        config.save(edit=False)

    def weboob_backend_get_params(self, backend_name, wb=None):
        if wb is None:
            wb = self.core.call_success("weboob_get")
        wb.load_backends(names=[backend_name])
        (module_name, params) = wb.backends_config.get_backend(backend_name)
        return (module_name, params)

    def weboob_backend_get(self, backend_name, wb=None):
        if wb is None:
            wb = self.core.call_success("weboob_get")
        wb.load_backends(names=[backend_name])
        return wb[backend_name]

    def weboob_backends_list(self, out: set, wb=None):
        if wb is None:
            wb = self.core.call_success("weboob_get")

        backends = sorted(list(wb.backends_config.iter_backends()))
        for (backend_name, module_name, params) in backends:
            minfo = wb.repositories.get_module_info(module_name)
            if minfo is None:
                LOGGER.warning(
                    "Backend %s configured but no correspnding module %s ?!",
                    backend_name, module_name
                )
                continue
            if not minfo.has_caps(modules.REQUIRED_CAPS):
                # Did someone install it ?
                LOGGER.warning(
                    "Backend '%s' (%s) doesn't have the bank capability",
                    backend_name, module_name
                )
                continue
            out.add(backend_name)

    def weboob_backend_delete(self, backend_name):
        wb = self.core.call_success("weboob_get")
        wb.backends_config.remove_backend(backend_name)
