import logging

import weboob.exceptions

import openpaperwork_core
import openpaperwork_core.promise

from .. import _


LOGGER = logging.getLogger(__name__)

KEY_REQUEST_INFORMATION = 'request_information'


class Progress(object):
    def __init__(self, nb_backends):
        self.nb_backends = nb_backends
        self.nb_accounts = 0
        self.nb_transactions = 0
        self.current_backend = -1
        self.current_account = -1
        self.current_transaction = -1

    def next_backend(self, nb_accounts):
        p = Progress(self.nb_backends)
        p.nb_accounts = nb_accounts
        p.current_backend = self.current_backend + 1
        return p

    def next_account(self, nb_transactions):
        p = Progress(self.nb_backends)
        p.nb_accounts = self.nb_accounts
        p.nb_transactions = nb_transactions
        p.current_backend = self.current_backend
        p.current_account = self.current_account + 1
        p.current_transaction = 0
        return p

    def next_transaction(self):
        # avoid creating useless objects too often
        self.current_transaction += 1
        return self

    def get_progress(self):
        # avoid divisions by 0
        nb_backends = self.nb_backends if self.nb_backends > 0 else -1
        nb_accounts = self.nb_accounts if self.nb_accounts > 0 else -1
        nb_transactions = (
            self.nb_transactions if self.nb_transactions > 0 else -1
        )

        backend_progress = self.current_backend / nb_backends
        account_progress = self.current_account / nb_accounts
        transaction_progress = self.current_transaction / nb_transactions
        return (
            backend_progress +
            (account_progress / nb_backends) +
            (transaction_progress / nb_accounts / nb_backends)
        )


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['weboob_account']

    def get_deps(self):
        return [
            {
                'interface': 'accounts',
                'defaults': ['pythoness.model.accounts'],
            },
        ]

    def _weboob_account_dump(self, wb, progress, backend, account):
        model_account = self.core.call_success(
            "account_new",
            account_id=account.id + "@" + backend.name,
            label=account.label,
            current_balance=account.balance
        )
        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_import_account_start", model_account
        )

        transactions = []
        try:
            for t in backend.iter_history(account):
                transactions.append(t)
        except Exception as exc:
            LOGGER.error(
                "Failed to import from %s/%s", backend, account,
                exc_info=exc
            )

        LOGGER.info(
            "Importing from %s/%s (%d transactions)",
            backend.name, account.label, len(transactions)
        )

        progress = progress.next_account(len(transactions))
        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_progress", "weboob", progress.get_progress(),
            _("Importing from account {backend}/{account}").format(
                backend=backend.name, account=account.label
            )
        )

        for transaction in transactions:
            progress = progress.next_transaction()
            self.core.call_success(
                "mainloop_schedule", self.core.call_all,
                "on_progress", "weboob", progress.get_progress(),
                _("Importing from account {backend}/{account}").format(
                    backend=backend.name, account=account.label
                )
            )

            dates = (transaction.vdate, transaction.date,)
            for date in dates:
                if not hasattr(date, 'year'):
                    continue
                break
            else:
                LOGGER.warning(
                    "Ignoring transaction because"
                    " it has no valid date: %s", transaction
                )
                continue

            transaction = self.core.call_success(
                "transaction_new",
                account=model_account,
                transaction_id=None,
                label=transaction.label,
                vdate=date,
                amount=float(transaction.amount),
                extras={"raw": transaction.raw}
            )
            self.core.call_success(
                "mainloop_execute", self.core.call_all,
                "on_import_transaction", transaction
            )

        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_import_account_end", model_account
        )

        return progress

    @staticmethod
    def _field_to_str(field):
        if hasattr(field, 'description'):
            return field.description
        if hasattr(field, 'id'):
            return field.id
        return str(field)

    def _weboob_account_dump_backend(self, progress, wb, backend):
        try:
            try:
                accounts = list(backend.iter_accounts())
            except weboob.exceptions.BrowserQuestion as exc:
                LOGGER.info(
                    "Got a question for the user from Weboob when using"
                    " backend %s", str(backend)
                )
                out = {}
                self.core.call_success(
                    "ask_user", out,
                    _("Backend {} needs to know:").format(str(backend)),
                    [(f.id, self._field_to_str(f)) for f in exc.fields]
                )
                if len(out) <= 0:
                    LOGGER.error("No reply from the user")
                    return progress
                for (k, v) in out.items():
                    backend.config[k].set(v)
                try:
                    accounts = list(backend.iter_accounts())
                except Exception:
                    # retry ...
                    accounts = list(backend.iter_accounts())
        except Exception as exc:
            LOGGER.error("Failed to import from %s", backend, exc_info=exc)
            return progress

        LOGGER.info("Importing from backend '%s'", backend.name)

        progress = progress.next_backend(len(accounts))
        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_progress", "weboob", progress.get_progress(),
            _("Importing from backend {}").format(backend.name)
        )

        for account in accounts:
            progress = self._weboob_account_dump(
                wb, progress, backend, account
            )
        return progress

    def weboob_account_dump_all_promise(self):
        wb = self.core.call_success("weboob_get")

        promise = openpaperwork_core.promise.Promise(
            self.core, self.core.call_all, args=("on_import_start",)
        )
        promise = promise.then(lambda *args, **kwargs: None)
        promise = promise.then(
            self.core.call_all, "on_progress",
            "weboob", 0.0, _("Importing data ...")
        )

        backends = set()
        self.core.call_success("weboob_backends_list", backends, wb)

        progress = Progress(len(backends))
        promise = promise.then(lambda *args, **kwargs: progress)

        for backend in backends:
            backend = self.core.call_success("weboob_backend_get", backend)

            # tell the backend we are interactive
            if KEY_REQUEST_INFORMATION in backend.config:
                backend.config[KEY_REQUEST_INFORMATION].set({})

            promise = promise.then(
                openpaperwork_core.promise.ThreadedPromise(
                    self.core, self._weboob_account_dump_backend,
                    args=(wb, backend)
                )
            )

        promise = promise.then(lambda *args, **kwargs: None)
        promise = promise.then(self.core.call_all, "on_import_end")
        promise = promise.then(lambda *args, **kwargs: None)
        promise = promise.then(
            self.core.call_all, "on_progress", "weboob", 1.0
        )
        promise = promise.then(lambda *args, **kwargs: None)
        return promise
