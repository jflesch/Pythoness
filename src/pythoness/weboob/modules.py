import weboob.capabilities.bank
import weboob.core.repositories

import openpaperwork_core


REQUIRED_CAPS = (
    weboob.capabilities.bank.CapBank,
)


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['weboob_modules']

    def get_deps(self):
        return [
            {
                'interface': 'weboob',
                'defaults': ['pythoness.weboob'],
            },
        ]

    def weboob_modules_list(self, out: set, installed=None):
        wb = self.core.call_success("weboob_get")
        modules = wb.repositories.get_all_modules_info()

        for (name, minfo) in modules.items():
            if not minfo.has_caps(REQUIRED_CAPS):
                continue
            if installed is not None and minfo.is_installed() != installed:
                continue
            out.add(name)

    def weboob_module_install(self, module_name):
        wb = self.core.call_success("weboob_get")

        minfo = wb.repositories.get_module_info(module_name)
        if minfo.is_installed():
            raise Exception("module {} is already installed".format(
                module_name
            ))

        wb.repositories.install(module_name)

    def weboob_module_get_params(self, module_name):
        wb = self.core.call_success("weboob_get")
        module = wb.modules_loader.get_or_load_module(module_name)
        return module.config.keys()
