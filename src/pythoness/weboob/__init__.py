import datetime
import logging

import weboob.core
import weboob.core.repositories

import openpaperwork_core
import openpaperwork_core.promise


LOGGER = logging.getLogger(__name__)


class Progress(weboob.core.repositories.IProgress):
    def __init__(self, core):
        self.core = core

    def progress(self, percent, message):
        LOGGER.info(
            "Weboob update progression: %d%%, %s", percent * 100, message
        )
        self.core.call_success(
            "mainloop_schedule", self.core.call_all,
            "on_progress", "weboob", percent, message
        )

    def error(self, message):
        # TODO
        LOGGER.error("Weboob error: %s", message)
        pass

    def prompt(self, message):
        # TODO
        LOGGER.error("Weboob prompt: %s", message)
        return "Not implemented yet"


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['weboob']

    def get_deps(self):
        return [
            {
                'interface': 'config',
                'defaults': ['openpaperwork_core.config'],
            },
            {
                'interface': 'mainloop',
                'defaults': ['openpaperwork_core.mainloop.asyncio'],
            },
        ]

    def init(self, core):
        super().init(core)
        opt = self.core.call_success(
            "config_build_simple", "weboob", "last_update",
            lambda: datetime.date(year=1971, month=1, day=1)
        )
        self.core.call_all("config_register", "weboob_last_update", opt)

        now = datetime.date.today()
        last_update = self.core.call_success(
            "config_get", "weboob_last_update"
        )
        if hasattr(last_update, 'value'):
            last_update = last_update.value

        if now - last_update > datetime.timedelta(days=7):
            self.weboob_update_promise().schedule()

    def weboob_get(self):
        return weboob.core.Weboob()

    def weboob_get_iprogress(self):
        return Progress(self.core)

    def weboob_update(self):
        wb = self.weboob_get()
        wb.update(self.weboob_get_iprogress())
        self.core.call_all(
            "config_put", "weboob_last_update", datetime.date.today()
        )

    def weboob_update_promise(self):
        return openpaperwork_core.promise.ThreadedPromise(
            self.core, self.weboob_update
        )
