import logging
import threading

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.windows = []

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_ask_user',
            'gtk_window_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_gtk_window_opened(self, window):
        self.windows.append(window)

    def on_gtk_window_closed(self, window):
        self.windows.remove(window)

    def ask_user(self, out: dict, question, fields):
        event = threading.Event()

        self.core.call_success(
            "mainloop_schedule", self._ask_user,
            out, question, fields, event
        )
        event.wait()

    def _ask_user(self, out: dict, question, fields, event):
        LOGGER.info("Asking user for '%s'", question)

        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.weboob", "ask.glade"
        )

        label = widget_tree.get_object("question_label")
        label.set_text(question)

        field_list = widget_tree.get_object("fields")
        field_list.clear()
        for (f_id, f_name) in fields:
            field_list.append((f_id, "", f_name))

        value_column = widget_tree.get_object("field_values")
        value_column.connect("edited", self._on_edit, widget_tree)

        dialog = widget_tree.get_object("question_dialog")
        dialog.connect("response", self._on_response, out, event, widget_tree)
        dialog.connect("destroy", self._on_destroy, event)
        dialog.set_transient_for(self.windows[-1])
        dialog.set_visible(True)

    def _on_edit(self, cell_renderer, path, new_text, widget_tree):
        field_list = widget_tree.get_object("fields")
        field_list[path][1] = new_text

    def _on_destroy(self, dialog, event):
        LOGGER.info("Dialog closed")
        event.set()

    def _on_response(self, dialog, response_type, out, event, widget_tree):
        if response_type != Gtk.ResponseType.OK:
            LOGGER.info("User declined replying")
            dialog.destroy()
            event.set()
            return

        field_list = widget_tree.get_object("fields")
        for (f_id, f_value, f_name) in field_list:
            out[f_id] = f_value
        event.set()
        dialog.destroy()
