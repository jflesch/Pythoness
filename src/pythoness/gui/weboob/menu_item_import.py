import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -100

    def get_interfaces(self):
        return ['gtk_weboob_menu_item_import']

    def get_deps(self):
        return [
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.gui.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'weboob_account',
                'defaults': ['pythoness.gui.weboob.accounts'],
            },
        ]

    def on_initialized(self):
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.weboob", "menu_item_import.glade"
        )
        if widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        menu_item = widget_tree.get_object("menu_item")
        menu_item.connect("activate", self._on_activate)
        self.core.call_all("app_menu_add", menu_item)

    def _on_activate(self, menu_item):
        promise = self.core.call_success("weboob_account_dump_all_promise")
        promise.schedule()
