import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.windows = []

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_weboob_settings',
            'gtk_window_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'weboob_backends',
                'defaults': ['pythoness.weboob.backends'],
            },
            {
                'interface': 'weboob_modules',
                'defaults': ['pythoness.weboob.modules'],
            },
        ]

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_gtk_window_opened(self, window):
        self.windows.append(window)

    def on_gtk_window_closed(self, window):
        self.windows.remove(window)

    def _reload_backends(self, widget_tree):
        backends = set()
        self.core.call_success("weboob_backends_list", backends)
        backends = list(backends)
        backends.sort()

        backend_list = widget_tree.get_object("settings_source_list")
        backend_list.remove_all()
        backend_list.append(None, _("New source"))
        for backend in backends:
            backend_list.append(backend, backend)
        backend_list.set_active(0)
        backend_list.connect("changed", self._on_backend_selected, widget_tree)

    def gtk_weboob_settings_open(self):
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.weboob", "settings.glade"
        )

        backend_list = widget_tree.get_object("settings_source_list")
        self._reload_backends(widget_tree)

        modules = set()
        self.core.call_success("weboob_modules_list", modules)
        modules = list(modules)
        modules.sort()

        module_list = widget_tree.get_object("module_list")
        module_list.remove_all()
        for m in modules:
            module_list.append(m, m)
        module_list.set_active(-1)
        module_list.connect("changed", self._on_module_selected, widget_tree)

        delete_button = widget_tree.get_object("settings_button_remove")
        delete_button.connect("clicked", self._on_delete, widget_tree)
        apply_button = widget_tree.get_object("settings_button_apply")
        apply_button.connect("clicked", self._on_add, widget_tree)

        dialog = widget_tree.get_object("settings_dialog")
        dialog.connect("response", self._on_response, widget_tree)
        dialog.set_transient_for(self.windows[-1])
        dialog.set_visible(True)

        attribute_column = widget_tree.get_object("attribute_values")
        attribute_column.connect("edited", self._on_edit, widget_tree)

        self._on_backend_selected(backend_list, widget_tree)
        self._on_module_selected(module_list, widget_tree)

    def _on_response(self, dialog, response, widget_tree):
        if response == Gtk.ResponseType.APPLY:
            pass
        elif response == Gtk.ResponseType.DELETE_EVENT:
            pass
        else:
            LOGGER.warning("Unknown response: %s", response)

    def _on_edit(self, cell_renderer, path, new_text, widget_tree):
        params_list = widget_tree.get_object("settings_source_attributes")
        params_list[path][1] = new_text

    def _on_backend_selected(self, combobox, widget_tree):
        active = combobox.get_active_id()
        LOGGER.info("Backend selected: %s", active)

        module_list = widget_tree.get_object("module_list")
        params_list = widget_tree.get_object("settings_source_attributes_view")
        delete_button = widget_tree.get_object("settings_button_remove")
        apply_button = widget_tree.get_object("settings_button_apply")

        if active is None:
            module_list.set_sensitive(True)
            params_list.set_sensitive(True)
            apply_button.set_sensitive(True)
            delete_button.set_sensitive(False)

            module_list.set_active(-1)
        else:
            module_list.set_sensitive(False)
            params_list.set_sensitive(False)
            apply_button.set_sensitive(False)
            delete_button.set_sensitive(True)

            (module_name, params) = self.core.call_success(
                "weboob_backend_get_params", active
            )
            LOGGER.info("Backend module: %s", module_name)
            module_list = widget_tree.get_object("module_list")
            for (idx, module_n) in enumerate(module_list.get_model()):
                if module_n[0] != module_name:
                    continue
                module_list.set_active(idx)
                break

    def _on_module_selected(self, combobox, widget_tree):
        module_list = widget_tree.get_object("module_list")
        backend_list = widget_tree.get_object("settings_source_list")
        active_backend = backend_list.get_active_id()
        active_module = module_list.get_active_id()

        if active_backend is not None:
            (module_name, params) = self.core.call_success(
                "weboob_backend_get_params", active_backend
            )
        elif active_module is not None:
            LOGGER.info("Module selected: %s", active_module)
            try:
                self.core.call_success("weboob_module_install", active_module)
            except Exception:
                # assume already installed
                pass
            params = self.core.call_success(
                "weboob_module_get_params", active_module
            )
            params = {k: "" for k in params}
        else:
            params = {}

        LOGGER.info("%d parameters", len(params))

        params_list = widget_tree.get_object("settings_source_attributes")
        params_list.clear()
        if active_backend is None and active_module is not None:
            params_list.append(("backend_name", "", _("Source name")))
        for (k, v) in params.items():
            params_list.append((k, v, k))

    def _on_delete(self, button, widget_tree):
        backend_list = widget_tree.get_object("settings_source_list")
        active_backend = backend_list.get_active_id()
        self.core.call_all("weboob_backend_delete", active_backend)
        self._reload_backends(widget_tree)

    def _on_add(self, button, widget_tree):
        module_list = widget_tree.get_object("module_list")
        active_module = module_list.get_active_id()

        params_list = widget_tree.get_object("settings_source_attributes")
        params = {}
        for (k, v, nice_name) in params_list:
            if v.strip() == "":
                continue
            params[k] = v
        backend_name = params.pop('backend_name')

        if backend_name.strip() == "":
            LOGGER.error("No backend name. Can't continue")
            return

        self.core.call_success(
            "weboob_backend_create", active_module, backend_name, params
        )

        self._reload_backends(widget_tree)
        backend_list = widget_tree.get_object("settings_source_list")
        for (idx, backend_n) in enumerate(backend_list.get_model()):
            if backend_n[0] != backend_name:
                continue
            backend_list.set_active(idx)
            break
        else:
            backend_list.set_active(-1)
