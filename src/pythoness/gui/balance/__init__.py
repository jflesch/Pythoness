import datetime
import math
import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps


LOGGER = logging.getLogger(__name__)


class BalanceEditor(object):
    def __init__(self, plugin, account, vdate):
        self.plugin = plugin
        self.core = plugin.core

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree", "pythoness.gui.balance", "balance.glade"
        )

        accounts = []
        self.core.call_all("db_account_get_all", accounts)
        accounts.sort(key=lambda a: a.label.lower())
        account_list = self.widget_tree.get_object("account")
        for (idx, laccount) in enumerate(accounts):
            backend = laccount.account_id.split("@", 1)
            if len(backend) <= 1:
                backend = ""
            else:
                backend = " ({})".format(backend[1])
            account_list.append(
                laccount.account_id,
                laccount.label + backend,
            )
        if account is not None:
            account_list.set_active_id(account.account_id)

        date = self.widget_tree.get_object("date")
        self.core.call_all("gtk_calendar_add_popover", date)
        vdate = self.core.call_success("i18n_date_short", vdate)
        date.set_text(vdate)

        account_list.connect("changed", self._on_change)
        date.connect("changed", self._on_change)

        dialog = self.widget_tree.get_object("dialog")
        dialog.connect("response", self._on_response)

        self._on_change()

    def show(self):
        dialog = self.widget_tree.get_object("dialog")
        dialog.set_visible(True)

    def _get_active_account(self):
        account_list = self.widget_tree.get_object("account")
        account = account_list.get_active_id()
        if account is None or account == "":
            return None
        account = self.core.call_success("db_account_get", account)
        return account

    def _on_change(self, *args, **kwargs):
        date = self.widget_tree.get_object("date")
        date = date.get_text()
        date = self.core.call_success("i18n_parse_date_short", date)
        if date is None:
            return
        date = datetime.datetime.combine(
            date, datetime.datetime.max.time()
        )

        account = self._get_active_account()
        if account is None:
            vbalance = math.nan
        else:
            vbalance = self.core.call_success(
                "db_transactions_get_balance", account, date
            )
        balance = self.widget_tree.get_object("balance")
        balance.set_text("{:.2f}".format(vbalance))

    def _on_response(self, dialog, response_id):
        if (response_id != Gtk.ResponseType.ACCEPT and
                response_id != Gtk.ResponseType.OK and
                response_id != Gtk.ResponseType.YES and
                response_id != Gtk.ResponseType.APPLY):
            dialog.destroy()
            LOGGER.info("User cancelled")
            return

        account = self._get_active_account()
        date = self.widget_tree.get_object("date")
        date = date.get_text()
        date = self.core.call_success("i18n_parse_date_short", date)
        if date is None:
            LOGGER.error("Invalid date specified")
            return
        date = datetime.datetime.combine(
            date, datetime.datetime.max.time()
        )

        new_balance = self.widget_tree.get_object("balance")
        new_balance = float(new_balance.get_text())
        dialog.destroy()

        current_balance = self.core.call_success(
            "db_transactions_get_balance", account, date
        )

        diff_balance = new_balance - current_balance
        account.current_balance += diff_balance
        self.core.call_all("db_account_update", account)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.windows = []

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_balance_editor',
            'gtk_window_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'account_storage',
                'defaults': ['pythoness.sqlite.accounts'],
            },
            {
                'interface': 'gtk_calendar_popover',
                'defaults': ['openpaperwork_gtk.widgets.calendar'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['pythoness.sqlite.transactions'],
            },
        ]

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_gtk_window_opened(self, window):
        self.windows.append(window)

    def on_gtk_window_closed(self, window):
        self.windows.remove(window)

    def gtk_balance_edit(self, account=None, date=None):
        now = datetime.datetime.now()
        if date is None:
            date = now
        if hasattr(date, 'date'):
            date = date.date()
        editor = BalanceEditor(self, account, date)
        editor.show()
