import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -1000

    def get_interfaces(self):
        return ['auto_reopen_db']

    def get_deps(self):
        return [
            {
                'interface': 'config',
                'defaults': ['openpaperwork_core.config'],
            },
            {
                'interface': 'db_open',
                'defaults': ['pythoness.sqlite.db'],
            },
        ]

    def init(self, core):
        super().init(core)
        opt = self.core.call_success(
            "config_build_simple", "db", "last", lambda: None
        )
        self.core.call_all("config_register", "last_db", opt)

        value = self.core.call_success("config_get", "last_db")
        if value is None:
            return
        LOGGER.info("Reopening previously opened DB: %s", value)
        self.core.call_all("db_open", value)

    def db_open(self, file_url, *args, **kwargs):
        current = self.core.call_success("config_get", "last_db")
        if current == file_url:
            return
        self.core.call_all("config_put", "last_db", file_url)
        self.core.call_all("config_save")
