import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 1000

    def __init__(self):
        super().__init__()
        self.windows = []

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_menu_item_db_open',
            'gtk_window_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'db',
                'defaults': ['pythoness.sqlite.db'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.gui.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def on_initialized(self):
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.db", "open.glade"
        )
        if widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        menu_item = widget_tree.get_object("menu_item_new")
        menu_item.connect("activate", self._on_open, widget_tree)
        self.core.call_all("app_menu_add", menu_item)

        menu_item = widget_tree.get_object("menu_item_open")
        menu_item.connect("activate", self._on_open, widget_tree)
        self.core.call_all("app_menu_add", menu_item)

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_gtk_window_opened(self, window):
        self.windows.append(window)

    def on_gtk_window_closed(self, window):
        self.windows.remove(window)

    def _on_open(self, menu_item, widget_tree):
        new = menu_item == widget_tree.get_object("menu_item_new")
        dialog = Gtk.FileChooserDialog(
            _("Select a file or a directory to import"),
            self.windows[-1],
            Gtk.FileChooserAction.SAVE if new else Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
            )
        )
        dialog.set_local_only(True)
        dialog.connect("response", self._on_open_response, new)
        dialog.show_all()

    def _on_open_response(self, dialog, response_id, new):
        if response_id != Gtk.ResponseType.OK:
            LOGGER.info("Cancelled by user")
            dialog.destroy()
            return
        selected = dialog.get_uri()
        dialog.destroy()
        LOGGER.info("Opening database %s", selected)
        if new and self.core.call_success("fs_exists", selected) is not None:
            LOGGER.info(
                "File '%s' already exist. Asking confirmation", selected
            )
            msg = _("Are you sure you want to squish '{}' ?").format(
                selected
            )
            confirm = Gtk.MessageDialog(
                parent=self.windows[-1],
                flags=Gtk.DialogFlags.MODAL |
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                message_type=Gtk.MessageType.WARNING,
                buttons=Gtk.ButtonsType.YES_NO,
                text=msg
            )
            confirm.connect("response", self._really_open, selected)
            confirm.show_all()
            return
        Gtk.RecentManager().add_item(selected)
        self.core.call_all("db_open", selected)

    def _really_open(self, dialog, response, selected):
        if response != Gtk.ResponseType.YES:
            LOGGER.info("User said no")
            dialog.destroy()
            return
        dialog.destroy()
        self.core.call_all("fs_unlink", selected)
        self.core.call_all("db_open", selected)
        Gtk.RecentManager().add_item(selected)
