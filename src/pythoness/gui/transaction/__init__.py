import datetime
import logging
import math

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from ... import _


LOGGER = logging.getLogger(__name__)


class TransactionEditor(object):
    def __init__(self, plugin, transaction=None, account=None):
        self.plugin = plugin
        self.core = plugin.core

        if account is None and transaction is not None:
            account = transaction.account

        self.transaction = transaction

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.transaction", "transaction.glade"
        )

        accounts = []
        self.core.call_all("db_account_get_all", accounts)
        accounts.sort(key=lambda a: a.label.lower())
        account_list = self.widget_tree.get_object("account")
        for (idx, laccount) in enumerate(accounts):
            backend = laccount.account_id.split("@", 1)
            if len(backend) <= 1:
                backend = ""
            else:
                backend = " ({})".format(backend[1])
            account_list.append(
                laccount.account_id,
                laccount.label + backend,
            )
        if account is not None:
            account_list.set_active_id(account.account_id)
        if transaction is not None:
            account_list.set_sensitive(False)

        date = self.widget_tree.get_object("date")
        self.core.call_all("gtk_calendar_add_popover", date)
        if transaction is None:
            vdate = datetime.datetime.now()
        else:
            vdate = transaction.vdate
        vdate = self.core.call_success("i18n_date_short", vdate)
        date.set_text(vdate)

        label = self.widget_tree.get_object("label")
        if transaction is not None:
            label.set_text(transaction.label.upper())

        radio = self.widget_tree.get_object("radiobuttonTransactionAmount")
        radio.connect("toggled", self._on_amount_type_toggled)
        radio.set_active(True)
        self.widget_tree.get_object("amountTransaction").set_sensitive(True)

        amount_transaction = self.widget_tree.get_object("amountTransaction")
        amount_balance = self.widget_tree.get_object("amountBalance")
        if transaction is None:
            amount_transaction.set_text("{:.2f}".format(0))
        else:
            amount_transaction.set_text("{:.2f}".format(transaction.amount))
        self._on_amount_changed()

        account_list.connect("changed", self._on_amount_changed)
        date.connect("changed", self._on_amount_changed)
        amount_balance.connect("changed", self._on_amount_changed)
        amount_transaction.connect("changed", self._on_amount_changed)

        dialog = self.widget_tree.get_object("dialog")
        dialog.set_transient_for(self.plugin.windows[-1])
        if transaction is None:
            dialog.set_title(_("Add transaction"))
        else:
            dialog.set_title(_("Edit transaction"))
        dialog.connect("response", self._on_response)

    def show(self):
        dialog = self.widget_tree.get_object("dialog")
        dialog.set_visible(True)

    def _on_amount_type_toggled(self, radio):
        self.widget_tree.get_object("amountTransaction").set_sensitive(
            radio.get_active()
        )
        self.widget_tree.get_object("amountBalance").set_sensitive(
            not radio.get_active()
        )

    def _get_active_account(self):
        account_list = self.widget_tree.get_object("account")
        account = account_list.get_active_id()
        if account is None or account == "":
            return None
        account = self.core.call_success("db_account_get", account)
        return account

    def _amount_to_balance(self, vdate, amount):
        # we want the balance at the end of the day
        if hasattr(vdate, 'date'):
            vdate = vdate.date()
        vdate = datetime.datetime.combine(vdate, datetime.datetime.max.time())

        account = self._get_active_account()
        if account is None:
            return math.nan
        balance = self.core.call_success(
            "db_transactions_get_balance", account, vdate
        )
        original_balance = balance
        if self.transaction is not None and self.transaction.vdate > vdate:
            # remove the current transaction from the balance as it will be
            # replaced
            balance -= self.transaction.amount
        balance += amount
        LOGGER.info(
            "Amount to balance: %s: B:%f + A:%f --> B:%f",
            vdate, original_balance, amount, balance
        )
        return balance

    def _balance_to_amount(self, vdate, balance):
        # we want the balance at the end of the day
        if hasattr(vdate, 'date'):
            vdate = vdate.date()
        vdate = datetime.datetime.combine(vdate, datetime.datetime.max.time())

        account = self._get_active_account()
        if account is None:
            return math.nan
        previous_balance = self.core.call_success(
            "db_transactions_get_balance", account, vdate
        )
        original_balance = previous_balance
        if self.transaction is not None and self.transaction.vdate > vdate:
            # remove the current transaction from the balance as it will be
            # replaced
            previous_balance -= self.transaction.amount
        amount = balance - previous_balance
        LOGGER.info(
            "Balance to amount: %s: B:%f - P:%f --> A:%f",
            vdate, balance, original_balance, amount
        )
        return amount

    def _on_amount_changed(self, entry=None):
        date = self.widget_tree.get_object("date").get_text()
        date = self.core.call_success("i18n_parse_date_short", date)
        if date is None:
            return

        radio = self.widget_tree.get_object("radiobuttonTransactionAmount")
        amount_transaction = self.widget_tree.get_object("amountTransaction")
        amount_balance = self.widget_tree.get_object("amountBalance")

        if radio.get_active():
            amount = amount_transaction.get_text()
            try:
                amount = float(amount)
            except ValueError:
                return
            balance = self._amount_to_balance(date, amount)
            amount_balance.set_text("{:.2f}".format(balance))
        else:
            balance = amount_balance.get_text()
            try:
                balance = float(balance)
            except ValueError:
                return
            amount = self._balance_to_amount(date, balance)
            amount_transaction.set_text("{:.2f}".format(amount))

    def _on_response(self, dialog, response_id):
        if (response_id != Gtk.ResponseType.ACCEPT and
                response_id != Gtk.ResponseType.OK and
                response_id != Gtk.ResponseType.YES and
                response_id != Gtk.ResponseType.APPLY):
            dialog.destroy()
            LOGGER.info("User cancelled")
            return

        # make sure the amount is up-to-date (if based on the balance)
        self._on_amount_changed()

        account = self._get_active_account()

        amount = float(
            self.widget_tree.get_object("amountTransaction").get_text()
        )
        label = self.widget_tree.get_object("label").get_text().upper().strip()
        date = self.widget_tree.get_object("date").get_text()
        dialog.destroy()

        vdate = self.core.call_success("i18n_parse_date_short", date)
        if vdate is None:
            LOGGER.error("Invalid date specified")
            return

        if self.transaction is not None:
            account.current_balance -= self.transaction.amount
            transaction = self.transaction
            transaction.label = label
            transaction.amount = amount
            transaction.vdate = vdate
            self.core.call_all("db_transaction_upd", transaction)
        else:
            transaction = self.core.call_success(
                "transaction_new",
                account=account,
                transaction_id="{}-{}-{}".format(date, label, amount),
                label=label,
                vdate=vdate,
                amount=amount
            )
            self.core.call_all("db_transactions_add", [transaction])

        account.current_balance += amount
        self.core.call_all("db_account_update", account)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.windows = []

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_transaction_editor',
            'gtk_window_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'account_storage',
                'defaults': ['pythoness.sqlite.accounts'],
            },
            {
                'interface': 'gtk_calendar_popover',
                'defaults': ['openpaperwork_gtk.widgets.calendar'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'i18n',
                'defaults': ['openpaperwork_core.i18n.python'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['pythoness.sqlite.transactions'],
            },
        ]

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_gtk_window_opened(self, window):
        self.windows.append(window)

    def on_gtk_window_closed(self, window):
        self.windows.remove(window)

    def gtk_transaction_add(self, account=None):
        editor = TransactionEditor(self, account=account)
        editor.show()

    def gtk_transaction_edit(self, transaction):
        editor = TransactionEditor(self, transaction=transaction)
        editor.show()
