import logging

try:
    from gi.repository import Gio
    from gi.repository import GLib
    GLIB_AVAILABLE = True
except (ImportError, ValueError):
    GLIB_AVAILABLE = False

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.stack_box = {}

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_app_menu',
            'gtk_mainwindow',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'fs',
                'defaults': ['openpaperwork_gtk.fs.gio'],
            },
            {
                'interface': 'gtk_progress_widget',
                'defaults': ['openpaperwork_gtk.widgets.progress'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.mainwindow", "mainwindow.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        self.mainwindow = self.widget_tree.get_object("mainwindow")
        self.mainwindow.connect("destroy", self._on_destroy)

        if hasattr(GLib, 'set_application_name'):
            GLib.set_application_name("Pythoness")
        GLib.set_prgname("pythoness")

        app = Gtk.Application(
            application_id=None,
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )
        app.register(None)
        Gtk.Application.set_default(app)
        self.mainwindow.set_application(app)

        headerbar = self.widget_tree.get_object("headerbar")
        headerbar.pack_end(
            self.core.call_success("gtk_progress_make_widget")
        )

    def chkdeps(self, out: dict):
        if not GLIB_AVAILABLE:
            out['glib'].update(openpaperwork_gtk.deps.GLIB)
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_initialized(self):
        mainwindow = self.widget_tree.get_object("mainwindow")
        mainwindow.set_visible(True)
        self.core.call_all("on_gtk_window_opened", mainwindow)

    def on_quit(self):
        if self.widget_tree is not None:
            mainwindow = self.widget_tree.get_object("mainwindow")
            mainwindow.set_visible(False)
            self.widget_tree = None

    def _on_destroy(self, mainwindow):
        LOGGER.info("Main window destroy. Quitting")
        self.core.call_all("on_gtk_window_closed", mainwindow)
        self.core.call_all("on_quit")
        self.core.call_all("mainloop_quit_graceful")

    def mainwindow_add(self, name, title, widget, expand=False):
        box = self.stack_box.get(name, None)
        if box is None:
            box = Gtk.Box.new(Gtk.Orientation.VERTICAL, spacing=5)
            box.set_visible(True)
            self.stack_box[name] = box
            stack = self.widget_tree.get_object("main_stack")
            stack.add_titled(box, name, title)

        box.pack_start(widget, expand=expand, fill=True, padding=5)

    def mainwindow_add_left(self, widget):
        box = self.widget_tree.get_object("main_box")
        box.pack_start(widget, expand=False, fill=False, padding=0)

    def app_menu_add(self, menu_item):
        menu = self.widget_tree.get_object("app_menu")
        menu.append(menu_item)

    def db_open(self, file_url, *args, **kwargs):
        headerbar = self.widget_tree.get_object("headerbar")
        headerbar.set_subtitle(self.core.call_success("fs_unsafe", file_url))
