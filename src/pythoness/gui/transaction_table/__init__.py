import logging

import openpaperwork_core

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -100

    def get_interfaces(self):
        return [
            'gtk_transaction_table',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_balance_editor',
                'defaults': ['pythoness.gui.balance'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'gtk_transaction_editor',
                'defaults': ['pythoness.gui.transaction'],
            },
            {
                'interface': 'gtk_transactions',
                'defaults': ['pythoness.gui.transactions'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['pythoness.sqlite.transactions'],
            },
        ]

    def on_initialized(self):
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.transaction_table", "transaction_table.glade"
        )
        if widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        widget_tree.get_object("button_add").connect(
            "clicked", self._on_add, widget_tree
        )
        widget_tree.get_object("button_edit").connect(
            "clicked", self._on_edit, widget_tree
        )
        widget_tree.get_object("button_remove").connect(
            "clicked", self._on_delete, widget_tree
        )
        widget_tree.get_object("button_balance").connect(
            "clicked", self._on_balance, widget_tree
        )

        table = widget_tree.get_object("table_timeline")
        table.set_model(self.core.call_success("timeline_get_liststore"))

        widget = widget_tree.get_object("timeline")
        self.core.call_all(
            "mainwindow_add", "timeline", _("Timeline"), widget, expand=True
        )

    def _get_selected(self, widget_tree):
        table = widget_tree.get_object("table_timeline")
        (model, rows) = table.get_selection().get_selected_rows()
        if len(rows) <= 0:
            return None
        row = model[rows[0]]
        transaction_id = row[0]
        return self.core.call_success("db_transaction_get", transaction_id)

    def _on_add(self, button, widget_tree):
        selected = self._get_selected(widget_tree)
        self.core.call_success(
            "gtk_transaction_add",
            selected.account if selected is not None else None
        )

    def _on_edit(self, button, widget_tree):
        selected = self._get_selected(widget_tree)
        if selected is None:
            LOGGER.warning("No transaction selected")
            return
        self.core.call_success("gtk_transaction_edit", selected)

    def _on_delete(self, button, widget_tree):
        selected = self._get_selected(widget_tree)
        if selected is None:
            LOGGER.warning("No transaction selected")
            return
        self.core.call_success(
            "gtk_show_dialog_yes_no", self, _("Are you sure ?"), selected
        )

    def _on_balance(self, button, widget_tree):
        selected = self._get_selected(widget_tree)
        self.core.call_success(
            "gtk_balance_edit",
            selected.account if selected is not None else None,
            selected.vdate if selected is not None else None,
        )

    def on_dialog_yes_no_reply(self, origin, reply, *args, **kwargs):
        if origin is not self:
            return
        if not reply:
            return
        (selected,) = args
        self.core.call_all("db_transaction_del", selected)
