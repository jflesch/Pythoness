import datetime
import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -100

    def __init__(self):
        super().__init__()
        self.liststore = None
        self.date_range = (None, None)
        self.keywords = None
        self.accounts = set()

    def get_interfaces(self):
        return [
            'gtk_transactions',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['pythoness.sqlite.transactions'],
            },
        ]

    def init(self, core):
        super().init(core)
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.transactions", "transactions.glade"
        )
        if widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return
        self.liststore = widget_tree.get_object("liststore_timeline")

    def timeline_get_liststore(self):
        return self.liststore

    def set_visible_accounts(self, accounts):
        self.accounts = set(accounts)
        self._refresh()

    def set_filter_date_range(self, first, last):
        date_range = (first, last)
        if self.date_range != date_range:
            self.date_range = date_range
            self._refresh()

    def set_filter_keywords(self, keywords):
        if self.keywords != keywords:
            self.keywords = keywords
            self._refresh()

    def on_db_modified(self):
        self._refresh()

    def _compute_balances(self, db_transactions):
        balances = {}
        for account in self.accounts:
            balance = self.core.call_success(
                'db_transactions_get_balance', account, self.date_range[1]
            )
            if balance is None:
                balance = 0.0
            balances[account.account_id] = balance
        total = sum(balances.values())

        transactions = []
        for (idx, transaction) in enumerate(db_transactions):
            account_id = transaction.account.account_id
            balance = balances[account_id]
            transactions.append((transaction, balance, total))
            balances[account_id] = balance - transaction.amount
            total = total - transaction.amount
        return transactions

    def _refresh(self):
        if self.liststore is None:
            return

        transactions = []
        for account in self.accounts:
            self.core.call_all(
                "db_transactions_get_by_account",
                transactions, account,
                start_date=self.date_range[0],
                end_date=self.date_range[1]
            )

        transactions.sort(key=lambda t: t.vdate, reverse=True)
        transactions = self._compute_balances(transactions)

        now = datetime.datetime.now()

        transaction_list = self.liststore
        transaction_list.clear()
        for (transaction, balance, total) in transactions:
            display = True
            if self.keywords is not None:
                for keyword in self.keywords:
                    if keyword not in transaction.label:
                        display = False
                        break
                if not display:
                    continue

            account = transaction.account
            backend = account.account_id.split("@", 1)
            if len(backend) <= 1:
                backend = ""
            else:
                backend = " ({})".format(backend[1])
            transaction_list.append((
                transaction.transaction_id,  # 0
                transaction.vdate.timestamp(),  # 1
                transaction.vdate.strftime("%x"),  # 2
                account.label + backend,  # 3
                transaction.denominator,  # 4
                transaction.label.title(),  # 5
                transaction.amount,  # 6
                -transaction.amount,  # 7
                "{:.2f}".format(transaction.amount)  # 8
                if transaction.amount > 0 else "",
                "{:.2f}".format(transaction.amount)  # 9
                if transaction.amount < 0 else "",
                balance,  # 10
                "{:.2f}".format(balance),  # 11
                total,  # 12
                "{:.2f}".format(total),  # 13
                "black" if transaction.vdate <= now else "gray",  # 14
            ))
