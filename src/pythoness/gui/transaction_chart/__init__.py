import datetime
import logging
import math

import openpaperwork_core

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.widget_tree = None

    def get_interfaces(self):
        return ['gtk_transaction_chart']

    def get_deps(self):
        return [
            {
                'interface': 'gtk_charts_lines',
                'defaults': ['openpaperwork_gtk.widgets.charts.lines'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'gtk_transactions',
                'defaults': ['pythoness.gui.transactions'],
            },
        ]

    def on_initialized(self):
        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.transaction_chart", "transaction_chart.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        liststore = self.core.call_success("timeline_get_liststore")
        chart = self.core.call_success(
            "gtk_charts_lines_get",
            liststore,
            column_value_id_idx=0,
            column_line_id_idx=3,
            column_axis_x_values_idx=1,
            column_axis_y_values_idx=10,
            column_axis_x_names_idx=2,
            column_axis_y_names_idx=11,
            highlight_x_range=(datetime.datetime.now().timestamp(), math.inf)
        )

        box = self.widget_tree.get_object("chart_box")
        box.add(chart.chart_widget)
        box.add(chart.legend_widget)

        self.core.call_all(
            "mainwindow_add", "timeline", _("Timeline"), box, expand=False
        )
