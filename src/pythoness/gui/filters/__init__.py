import datetime
import logging

import openpaperwork_core

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 100

    def __init__(self):
        super().__init__()
        self.widget_tree = None

    def get_interfaces(self):
        return ['filters']

    def get_deps(self):
        return [
            {
                'interface': 'gtk_calendar_popover',
                'defaults': ['openpaperwork_gtk.widgets.calendar'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.mainwindow'],
            },
            {
                'interface': 'i18n',
                'defaults': ['openpaperwork_core.i18n.python'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['pythoness.sqlite.transactions'],
            },
        ]

    def on_initialized(self):
        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.filters", "filters.glade"
        )
        if self.widget_tree is None:
            return

        widget = self.widget_tree.get_object("filters")
        self.core.call_all(
            "mainwindow_add", "timeline", _("Timeline"), widget
        )

        now = datetime.datetime.now()
        first = now
        last = now + datetime.timedelta(days=183)  # ~6 months

        self.set_date_range(first, last)

        date_start = self.widget_tree.get_object("date_start")
        date_start.connect("changed", self._notify)
        self.core.call_all("gtk_calendar_add_popover", date_start)
        date_end = self.widget_tree.get_object("date_end")
        date_end.connect("changed", self._notify)
        self.core.call_all("gtk_calendar_add_popover", date_end)

        keywords = self.widget_tree.get_object("keywords")
        keywords.connect("search-changed", self._notify)

        self.on_db_modified()

    def set_date_range(self, first, last):
        first = self.core.call_success("i18n_date_short", first)
        last = self.core.call_success("i18n_date_short", last)
        self.widget_tree.get_object("date_start").set_text(first)
        self.widget_tree.get_object("date_end").set_text(last)

    def on_db_modified(self):
        if self.widget_tree is None:
            return

        first = self.core.call_success("db_transactions_get_first_vdate")
        if first is None:
            return
        two_years_ago = (
            datetime.datetime.now() - datetime.timedelta(days=(2 * 365))
        )
        first = max(first, two_years_ago)

        first = self.core.call_success("i18n_date_short", first)
        date_start = self.widget_tree.get_object("date_start")
        if date_start.get_text() == first:
            return
        date_start.set_text(first)
        self._notify()

    def _notify(self, *args, **kwargs):
        first_str = self.widget_tree.get_object("date_start").get_text()
        last_str = self.widget_tree.get_object("date_end").get_text()
        first = self.core.call_success("i18n_parse_date_short", first_str)
        last = self.core.call_success("i18n_parse_date_short", last_str)
        if first is None or last is None:
            return
        keywords = self.widget_tree.get_object("keywords").get_text()
        if keywords.strip() == "":
            keywords = None
        else:
            keywords = keywords.upper().split()
            keywords.sort()
        first = datetime.datetime.combine(first, datetime.datetime.min.time())
        last = datetime.datetime.combine(last, datetime.datetime.max.time())
        self.core.call_all("set_filter_date_range", first, last)
        self.core.call_all("set_filter_keywords", keywords)
