import openpaperwork_core


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        self.current_transactions = []

    def get_interfaces(self):
        return ['import']

    def get_deps(self):
        return [
            {
                'interface': 'account_storage',
                'defaults': ['sqlite.accounts'],
            },
            {
                'interface': 'transaction_storage',
                'defaults': ['sqlite.transactions'],
            },
        ]

    def on_import_start(self):
        self.current_transactions = []

    def on_import_account_start(self, account):
        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "db_account_add", account
        )

    def on_import_transaction(self, transaction):
        self.current_transactions.append(transaction)

    def on_import_account_end(self, account):
        self.core.call_success(
            "mainloop_execute", self.core.call_all,
            "db_transactions_add",
            self.current_transactions
        )
        self.current_transactions = []

    def on_import_end(self):
        self.current_transactions = []
