#!/usr/bin/env python3

import sys

import setuptools

quiet = '--quiet' in sys.argv or '-q' in sys.argv

try:
    with open("src/pythoness/_version.py", "r") as file_descriptor:
        version = file_descriptor.read().strip()
        version = version.split(" ")[2][1:-1]
    if not quiet:
        print("Pythoness version: {}".format(version))
    if "-" in version:
        version = version.split("-")[0]
except FileNotFoundError:
    print("ERROR: _version.py file is missing")
    print("ERROR: Please run 'make version' first")
    sys.exit(1)

setuptools.setup(
    name="pythoness",
    version=version,
    description="Predictive personal finance manager",
    long_description="""Pythoness is a personal finance manager with
a strong focus on the future instead of the past.""",
    keywords="finance",
    url="https://framagit.org/OpenPaperwork/pythoness",
    download_url=(
        "https://framagit.org/OpenPaperwork/pythoness/-"
        "/archive/{}/pythoness-{}.tar.gz".format(version, version)
    ),
    classifiers=[
        "Intended Audience :: End Users/Desktop",
        (
            "License :: OSI Approved"
            " :: GNU Affero General Public License v3 or later (AGPLv3+)"
        ),
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
    ],
    license="AGPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@openpaper.work",
    packages=setuptools.find_packages('src'),
    include_package_data=True,
    package_dir={'': 'src'},
    zip_safe=True,
    install_requires=[
        "openpaperwork-core",
        "openpaperwork-gtk",
        "weboob",
    ],
    entry_points={
        'gui_scripts': [
            'pythoness = pythoness.main:main',
        ],
    },
)

if quiet:
    sys.exit(0)

print("============================================================")
print("============================================================")
print("||                       IMPORTANT                        ||")
print("||            Please run 'pythoness chkdeps'              ||")
print("||            to find any missing dependency              ||")
print("============================================================")
print("============================================================")
