VERSION_FILE = src/pythoness/_version.py
PYTHON ?= python3

build: build_c build_py

install: install_py install_c

uninstall: uninstall_py

build_py: ${VERSION_FILE} l10n_compile
	${PYTHON} ./setup.py build

build_c:

version: ${VERSION_FILE}

${VERSION_FILE}:
	echo -n "version = \"" >| $@
	echo -n $(shell git describe --always) >> $@
	echo "\"" >> $@

doc: install_py
	$(MAKE) -C doc html

doc/_build/html/index.html: doc

upload_doc: doc/_build/html/index.html
	./sub/paperwork/ci/deliver_doc.sh \
		${CURDIR}/doc/_build/html \
		pythoness

data:

check:
	flake8 src/pythoness

test: install
	python3 -m unittest discover --verbose -s tests

linux_exe:

release:
ifeq (${RELEASE}, )
	@echo "You must specify a release version (make release RELEASE=1.2.3)"
else
	@echo "Will release: ${RELEASE}"
	@echo "Checking release is in ChangeLog ..."
	grep ${RELEASE} ChangeLog | grep -v "/xx"
endif

release_pypi:
	@echo "Releasing pythoness ..."
	${PYTHON} ./setup.py sdist
	twine upload dist/pythoness-${RELEASE}.tar.gz
	@echo "All done"

clean:
	rm -f ${VERSION_FILE}
	rm -rf build dist *.egg-info
	rm -rf venv

# PIP_ARGS is used by Flatpak build
install_py: ${VERSION_FILE} l10n_compile
	(cd sub/weboob && python3 ./setup.py install)
	$(MAKE) -C sub/paperwork/openpaperwork-core install_py
	$(MAKE) -C sub/paperwork/openpaperwork-gtk install_py
	${PYTHON} ./setup.py install ${PIP_ARGS}

install_c:

uninstall_py:
	pip3 uninstall -y pythoness

uninstall_c:

l10n_extract:
	$(CURDIR)/sub/paperwork/tools/l10n_extract.sh "$(CURDIR)/src" "$(CURDIR)/l10n"

l10n_compile:
	# TODO
	# $(CURDIR)/sub/paperwork/tools/l10n_compile.sh \
	# 	"$(CURDIR)/l10n" \
	# 	"$(CURDIR)/src/pythoness/l10n" \
	# 	"pythoness"

help:
	@echo "make build || make build_py"
	@echo "make check"
	@echo "make help: display this message"
	@echo "make install || make install_py"
	@echo "make uninstall || make uninstall_py"
	@echo "make release"

.PHONY: \
	build \
	build_c \
	build_py \
	check \
	data \
	doc \
	exe \
	help \
	install \
	install_c \
	install_py \
	l10n_compile \
	l10n_extract \
	release \
	test \
	upload_data \
	upload_doc \
	uninstall \
	uninstall_c \
	version
